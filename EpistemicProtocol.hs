module EpistemicProtocol
where

import Data.Map as Map
import Data.List as List

import DEMO_S5_extended
import RcpCalculations as Calc

noCrossingHandsSuccess :: EpistM Int -> Form Int -> Bool
noCrossingHandsSuccess m an = isTrue m (Pub an $ CK ags (Conj [b_knows_as m, eveIgnorant m])) &&
                              isTrue m (Pub (announceEvesCards m (Ag 1)) $ Pub an $ CK ags (eveIgnorant m))
  where (Mo _ ags _ _ _) = m
-- Intermediate assumptions for three-agent RCP:
-- (Intermediate being assumptions that are being used before the protocol has ended.)


onlySafetySuccess :: EpistM Int -> Form Int -> Bool
onlySafetySuccess m an = isTrue m (Pub an $ CK ags (eveIgnorant m)) &&
                         isTrue m (Pub (announceEvesCards m (Ag 1)) $ Pub an $ CK ags (eveIgnorant m))
  where (Mo _ ags _ _ _) = m


onlySafetySuccessAfterFirst :: EpistM Int -> Form Int -> Bool
onlySafetySuccessAfterFirst m an = isTrue m (Pub an $ CK ags (eveIgnorant m))
  where (Mo _ ags _ _ _) = m

-- Agent knows that the announcement will result in common knowledge that Eve is ignorant.
checkAgentKnowsSafety :: EpistM Int -> Form Int -> Agent  -> Bool
checkAgentKnowsSafety m an ag = isTrue m (Kn ag (Pub an (CK ags $ eveIgnorant m)))
  where (Mo _ ags _ _ _) = m

addAgentKnowsSafetyToAnnouncement :: EpistM Int -> Form Int  -> Form Int
addAgentKnowsSafetyToAnnouncement m an = Conj [an, Pub an (CK ags $ eveIgnorant m)]
  where (Mo _ ags _ _ _) = m

-- It must be common knowledge between all agents that after the announcement it is common knowledge that eve is ignorant.
commonKnowledgeOfSafety :: EpistM Int -> Form Int -> Bool
commonKnowledgeOfSafety m an = isTrue m $ CK ags (Pub an (CK ags $ eveIgnorant m))
  where (Mo _ ags _ _ _) = m

-- Ann knows that after her announcement, it is common knowledge between the protagonists that b knows a's cards.
aKnowsInformativity :: EpistM Int -> Form Int -> Bool
aKnowsInformativity m an = agentKnowsInformativity m an (Ag 0)

agentKnowsInformativity :: EpistM Int -> Form Int -> Agent -> Bool
agentKnowsInformativity m an ag = isTrue m (Kn ag (Pub an (CK comAgs $ b_knows_as m)))
  where (Mo _ ags _ _ _) = m
        comAgs = take (length ags - 1) ags

-- Weaker version:
-- A knows that after her announcement, Bill will know her cards. (It is no longer common knowledge).
-- Can it be proved that the above and below are equivalent? If so, that will help model-checking speed significantly.
-- I know that the formulas are not equivalent, but they might be in this domain?
aKnowsInformativityWeak :: EpistM Int -> Form Int -> Bool
aKnowsInformativityWeak m an = agentKnowsInformativityWeak m an (Ag 0)

agentKnowsInformativityWeak :: EpistM Int -> Form Int -> Agent -> Bool
agentKnowsInformativityWeak m an ag = isTrue m (Kn ag (Pub an (b_knows_as m)))

-- Final postcondition as in "Model Checking Russian Cards" page 111:
postconditionsRCP :: EpistM Int -> Bool
postconditionsRCP mod = isTrue mod $ Conj [CK protagonists $ Conj [a_knows_bs mod, b_knows_as mod], CK ags $ eveIgnorant mod]
  where (Mo _ ags _ _ _) = mod
        protagonists = take (length ags - 1) ags

-- Helper functions to check for informativity (or lack of it).
-- See if the Eavesdropper is ignorant in an epistemic model.
eveIgnorant :: EpistM Int -> Form a
eveIgnorant (Mo _ ags val _ [st]) = Conj [Conj[Ng (Kn eve (Prp (Prop x i))) | x <- protagonists] | i <-[0..length v - 1]]
  where v = val ! st
        eve = last ags
        protagonists = take (length ags - 1) ags

-- Formulas to present agents' knowledge.
-- Anne knows Bill's cards
a_knows_bs :: EpistM Int -> Form a
a_knows_bs m = agentKnowsAgentsCards m a b

-- Bill knows Anne's cards
b_knows_as :: EpistM Int -> Form a
b_knows_as m =  agentKnowsAgentsCards m b a

-- Cath knows Anne's cards (For the 4 agent case)
c_knows_as :: EpistM Int -> Form a
c_knows_as m =  agentKnowsAgentsCards m c a

agentKnowsAgentsCards :: EpistM Int -> Agent -> Agent -> Form a
agentKnowsAgentsCards (Mo _ _ val _ [st]) ag1 ag2 = Conj[Disj[Kn ag1 (Prp (Prop ag2 i)), Kn ag1 (Ng(Prp (Prop ag2 i)))] | i<-[0..length v - 1]]
  where v = val ! st

-- It is common knowledge between all protagonists that they know each others cards.
comKnowsAll :: EpistM Int -> Bool
comKnowsAll mod = all (isTrue mod) [Conj[Disj[CK comAgs (Prp (Prop x i)), CK comAgs (Ng(Prp (Prop x i)))] | i<-[0..length v -1]] | x <- comAgs]
  where comAgs = take (length ags - 1) ags
        (Mo _ ags val _ [st]) = mod
        v = val ! st

-- BELOW ARE HELPER FUNCTIONS TO GENERATE THE EPISTEMIC PROTOCOL

announcementCombinations :: (Eq a) => Form a -> Int -> [Form a] -> [[Form a]]
announcementCombinations actualHand 0 _ = [[actualHand]] -- Add the actual hand at the last recursive step, less computing. Hand-combinations over lowerbound -1, instead of just lowerbound. (An announcement must be true, and therefore the actual hand must be in the announcement.)
announcementCombinations ah n xs = [y:ys | y:xs' <- List.tails xs, ys <- announcementCombinations ah (n-1) xs']

-- See fact 26 The Russian Cards Problem (Ditmarsch). The last announcement in the sequence can be the agent announcing Eve's cards. This is equivalent to an "alternative hands" formula.
announceEvesCards :: EpistM Int -> Agent -> Form a
announceEvesCards m agent = Kn agent $ Conj prpToFormEves
  where (Mo _ ag val _ [st]) = m
        valuation = Map.findWithDefault (error "Can't find valuationlist in current state") st val
        evesCards = List.filter (\(Prop x _) -> x == eve) valuation
        eve = last ag
        prpToFormEves = listOfPrpToForm evesCards

listOfPrpToForm :: [Prp] -> [Form a]
listOfPrpToForm = List.map Prp

--Use integerAnnouncementToProp to create an announcement from a list of numbers, such as: [[0,1,2],[1,2,3], [1,2,4]] etc. Makes it easier to test announcements by hand.
integerAnnouncementToProp :: [[Int]] -> Agent -> Form a
integerAnnouncementToProp altHands ag = Kn ag (Disj (List.map (\y -> singleIntegerHandToProp y ag) altHands))

singleIntegerHandToProp :: [Int] -> Agent -> Form a
singleIntegerHandToProp hand ag = Conj (List.map (\y -> intToProp y ag) hand)

intToProp :: Int -> Agent -> Form a
intToProp num ag = Prp (Prop ag num)

generateConjunctionsOfHands :: [Int] -> Agent -> [Form a]
generateConjunctionsOfHands cardDstr (Ag x) = [Conj y  | y <- generateAllHands (Ag x) (Calc.combinations (cardDstr !! x)  [0..sum cardDstr - 1])]

generateAllHands :: Agent -> [[Int]] -> [[Form a]]
generateAllHands agent = List.map (generateHand agent)

generateHand :: Agent -> [Int] -> [Form a]
generateHand agent numbers = [Prp (Prop agent x) | x <- numbers]

generateActualHand :: Agent -> EpistM Int -> Form a
generateActualHand ag (Mo _ _ val _ [st]) = Conj [ Prp y | y <- agentsCards ]
  where valuation = Map.findWithDefault (error "Can't find valuationlist in current state") st val
        agentsCards = List.filter (\(Prop agent _) -> agent == ag) valuation

changeCurrentStateInModel :: EpistM Int -> Int -> EpistM Int
changeCurrentStateInModel (Mo a b c d _) newState = (Mo a b c d [newState])

announcePossibleEveCards :: EpistM Int -> [Int] -> Agent -> EpistM Int
announcePossibleEveCards model cards agent = upd_pa model announcement
  where announcement = createDisjunctEveAnnouncement cards agent

createDisjunctEveAnnouncement :: [Int] -> Agent -> Form a
createDisjunctEveAnnouncement cards agent = Kn agent (Disj [Prp (Prop (Ag 2) x) | x <- cards])  