module RcpLatex
where

import Data.List as List

import DEMO_S5_extended

knowsToLatex :: Form a -> String
knowsToLatex (Kn (Ag x) h) = "$K_" ++ show (Ag x) ++ disjToLatex h ++ "$" ++ "\n"

disjToLatex :: Form a -> String
disjToLatex (Disj h) = "("++ List.intercalate (" \\" ++ "vee ") (List.map conjToLatex h) ++ ")"

conjToLatex :: Form a -> String
conjToLatex (Conj h) = List.intercalate "" (List.map prpToLatex h) ++ "_" ++ show (Ag x)
  where Prp (Prop (Ag x) _) = head h

prpToLatex :: Form a -> String
prpToLatex (Prp (Prop _ y)) = show y
