module RcpCalculations
where

import DEMO_S5_extended
import Control.Monad

import Data.List as List
import Data.Set as Set

powerset :: [a] -> [[a]]
powerset = filterM (const [True, False])

factorial :: (RealFrac a) => a -> a
factorial n = if n < 2 then 1 else n * factorial (n-1)

choose :: Int -> Int -> Int
choose _ 0 = 1
choose 0 _ = 0
choose n k = choose (n-1) (k-1) * n `div` k

hasTwoOfEachPair :: [Form a] -> Bool
hasTwoOfEachPair knowledge = all (\y -> List.length (List.filter (not . Set.isSubsetOf y) pairs) == 2) formToInt
  where formToInt = [Set.fromList [x | Prp (Prop _ x) <- y] | (Conj y) <- conjs]
        conjs = [l | Disj [l] <- knowsToDisj]
        knowsToDisj = [y | Kn _ y <- knowledge]
        pairs = List.map Set.fromList (combinations 2 [0..6])

combinations :: Int -> [a] -> [[a]]
combinations 0 _  = [ [] ]
combinations n xs = [ y:ys | y:xs' <- List.tails xs, ys <- combinations (n-1) xs']
