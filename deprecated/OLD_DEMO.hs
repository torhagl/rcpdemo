module DEMO
where

import List
import Char
import DPLL

version :: String
version = "DEMO 1.02, October 2004"

data Agent = A | B | C | D | E deriving (Eq,Ord,Enum)

a, alice, b, bob, c, carol, d, dave, e, ernie  :: Agent
a = A; alice = A
b = B; bob   = B
c = C; carol = C
d = D; dave  = D
e = E; ernie = E

instance Show Agent where
  show A = "a"; show B = "b"; show C = "c"; show D = "d" ; show E = "e"

all_agents :: [Agent]
all_agents = [a .. last_agent]

last_agent :: Agent
--last_agent = a
--last_agent = b
last_agent = c
--last_agent = d
--last_agent = e

data Prop = P Int | Q Int | R Int deriving (Eq,Ord)

instance Show Prop where
  show (P 0) = "p"; show (P i) = "p" ++ show i
  show (Q 0) = "q"; show (Q i) = "q" ++ show i
  show (R 0) = "r"; show (R i) = "r" ++ show i

data Form = Top
          | Prop Prop
          | Neg  Form
          | Conj [Form]
          | Disj [Form]
          | Pr Program Form
          | K Agent Form
          | EK [Agent] Form
          | CK [Agent] Form
          | Up PoAM Form
          | Aut (NFA State) Form
          deriving (Eq,Ord)

data Program = Ag Agent
             | Ags [Agent]
             | Test Form
             | Conc [Program]
             | Sum  [Program]
             | Star Program
             deriving (Eq,Ord)

impl :: Form -> Form -> Form
impl form1 form2 = Disj [Neg form1, form2]

instance Show Form where
  show Top = "T" ; show (Prop p) = show p; show (Neg f) = '-':(show f);
  show (Conj fs)     = '&': show fs
  show (Disj fs)     = 'v': show fs
  show (Pr p f)      = '[': show p ++ "]" ++ show f
  show (K agent f)   = '[': show agent ++ "]" ++ show f
  show (EK agents f) = 'E': show agents ++ show f
  show (CK agents f) = 'C': show agents ++ show f
  show (Up pam f)    = 'A': show (points pam) ++ show f
  show (Aut aut f)   = '[': show aut ++ "]" ++ show f

instance Show Program where
  show (Ag a)       = show a
  show (Ags as)     = show as
  show (Test f)     = '?': show f
  show (Conc ps)    = 'C': show ps
  show (Sum ps)     = 'U': show ps
  show (Star p)     = '(': show p ++ ")*"

splitU :: [Program] -> ([Form],[Agent],[Program])
splitU [] = ([],[],[])
splitU (Test f: ps) = (f:fs,ags,prs)
  where (fs,ags,prs) = splitU ps
splitU (Ag x: ps) = (fs,union [x] ags,prs)
  where (fs,ags,prs) = splitU ps
splitU (Ags xs: ps) = (fs,union xs ags,prs)
  where (fs,ags,prs) = splitU ps
splitU (Sum ps: ps') = splitU (union ps ps')
splitU (p:ps)        = (fs,ags,p:prs)
  where (fs,ags,prs) = splitU ps

comprC :: [Program] -> [Program]
comprC [] = []
comprC (Test Top: ps)          = comprC ps
comprC (Test (Neg Top): ps)    = [Test (Neg Top)]
comprC (Test f: Test f': rest) = comprC (Test (canonF (Conj [f,f'])): rest)
comprC (Conc ps : ps')         = comprC (ps ++ ps')
comprC (p:ps)                  = let ps' = comprC ps
                                 in
                                   if ps' == [Test (Neg Top)]
                                     then [Test (Neg Top)]
                                     else p: ps'

simpl :: Program -> Program
simpl (Ag x)                    = Ag x
simpl (Ags [])                  = Test (Neg Top)
simpl (Ags [x])                 = Ag x
simpl (Ags xs)                  = Ags xs
simpl (Test f)                  = Test (canonF f)

simpl (Sum prs) =
  let (fs,xs,rest) = splitU (map simpl prs)
      f            = canonF (Disj fs)
  in
    if xs == [] && rest == []
      then Test f
    else if xs == [] && f == Neg Top && length rest == 1
      then (head rest)
    else if xs == [] && f == Neg Top
      then Sum rest
    else if xs == []
      then Sum (Test f: rest)
    else if length xs == 1  && f == Neg Top
      then Sum (Ag (head xs): rest)
    else if length xs == 1
      then Sum (Test f: Ag (head xs): rest)
    else if f == Neg Top
      then Sum (Ags xs: rest)
    else Sum (Test f: Ags xs: rest)

simpl (Conc prs) =
    let prs' = comprC (map simpl prs)
    in
      if prs'== []                  then Test Top
      else if length prs' == 1      then head prs'
      else if head prs' == Test Top then Conc (tail prs')
      else                               Conc prs'

simpl (Star pr) = case simpl pr of
    Test f             -> Test Top
    Sum [Test f, pr']  -> Star pr'
    Sum (Test f: prs') -> Star (Sum prs')
    Star pr'           -> Star pr'
    pr'                -> Star pr'

pureProp ::  Form -> Bool
pureProp Top       = True
pureProp (Prop _)  = True
pureProp (Neg f)   = pureProp f
pureProp (Conj fs) = and (map pureProp fs)
pureProp (Disj fs) = and (map pureProp fs)
pureProp  _        = False

bot, p, q, r, p1, p2, p3, q1, q2, q3, r1, r2, r3 :: Form
bot = Neg Top
p   = Prop (P 0); q  = Prop (Q 0); r  = Prop (R 0)
p1  = Prop (P 1); p2 = Prop (P 2); p3 = Prop (P 3)
q1  = Prop (Q 1); q2 = Prop (Q 2); q3 = Prop (Q 3)
r1  = Prop (R 1); r2 = Prop (R 2); r3 = Prop (R 3)
u   = Up ::  PoAM -> Form -> Form

nkap = Neg (K a p)
nkanp = Neg (K a (Neg p))
nka_p = Conj [nkap,nkanp]

type State = Integer

data Model state formula = Mo
                           [state]
                           [(state,formula)]
                           [(Agent,state,state)]
                           deriving (Eq,Ord,Show)

data Pmod state formula = Pmod
                           [state]
                           [(state,formula)]
                           [(Agent,state,state)]
                           [state]
                           deriving (Eq,Ord,Show)

mod2pmod :: Model state formula -> [state] -> Pmod state formula
mod2pmod (Mo states prec accs) points = Pmod states prec accs points

pmod2mp :: Pmod state formula -> (Model state formula, [state])
pmod2mp (Pmod states prec accs points)  = (Mo states prec accs, points)

decompose ::  Pmod state formula -> [(Model state formula, state)]
decompose (Pmod states prec accs points) =
   [(Mo states prec accs, point) | point <- points ]

table2fct :: Eq a => [(a,b)] -> a -> b
table2fct t = \ x -> maybe undefined id (lookup x t)

domain :: Model state formula -> [state]
domain (Mo states _ _) = states

eval :: Model state formula -> [(state,formula)]
eval (Mo _ pre _) = pre

access :: Model state formula -> [(Agent,state,state)]
access (Mo _ _ rel) = rel

points :: Pmod state formula -> [state]
points (Pmod _ _ _ pnts) = pnts

gsm :: Ord state => Pmod state formula ->  Pmod state formula
gsm (Pmod states pre rel points) = (Pmod states' pre' rel' points)
   where
   states' = closure rel all_agents points
   pre'    = [(s,f)     | (s,f)     <- pre,
                           elem s states'                   ]
   rel'    = [(ag,s,s') | (ag,s,s') <- rel,
                           elem s states',
                           elem s' states'                  ]

closure ::  Ord state =>
             [(Agent,state,state)] -> [Agent] -> [state] -> [state]
closure rel agents xs
  | xs' == xs = xs
  | otherwise = closure rel agents xs'
     where
     xs' = (nub . sort) (xs ++ (expand rel agents xs))

expand :: Ord state =>
           [(Agent,state,state)] -> [Agent] -> [state] -> [state]
expand rel agnts ys =
      (nub . sort . concat)
         [ alternatives rel ag state | ag    <- agnts,
                                       state <- ys       ]

alternatives :: Eq state =>
                [(Agent,state,state)] -> Agent -> state -> [state]
alternatives rel ag current =
  [ s' | (a,s,s') <- rel, a == ag, s == current ]

type SM = Model State [Prop]

type EpistM = Pmod State [Prop]

type AM = Model State Form

type PoAM = Pmod State Form

preconditions :: PoAM -> [Form]
preconditions (Pmod states pre acc points) =
   map (table2fct pre) points

precondition :: PoAM -> Form
precondition am = canonF (Conj (preconditions am))

gsmPoAM :: PoAM -> PoAM
gsmPoAM (Pmod states pre acc points) =
  let
    points' = [ p | p <- points, consistent (table2fct pre p) ]
    states' = [ s | s <- states, consistent (table2fct pre s) ]
    pre'    = filter (\ (x,_) -> elem x states') pre
    f       = \ (_,s,t) -> elem s states' && elem t states'
    acc'    = filter f acc
  in
  if points' == []
     then (Pmod [] [] [] [])
     else gsm (Pmod states' pre' acc' points')

accFor :: Eq a => a -> [(a,b,b)] -> [(b,b)]
accFor label triples = [ (x,y) | (label',x,y) <- triples, label == label' ]

containedIn :: Eq a => [a] -> [a] -> Bool
containedIn [] ys     = True
containedIn (x:xs) ys = elem x ys && containedIn xs ys

idR :: Eq a => [a] -> [(a,a)]
idR = map (\x -> (x,x))

reflR :: Eq a => [a] -> [(a,a)] -> Bool
reflR xs r = containedIn (idR xs) r

symR :: Eq a => [(a,a)] -> Bool
symR [] = True
symR ((x,y):pairs) | x == y    = symR (pairs)
                   | otherwise = elem (y,x) pairs
                                 && symR (pairs \\ [(y,x)])

transR :: Eq a => [(a,a)] -> Bool
transR [] = True
transR s = and [ trans pair s | pair <- s ]
   where
   trans (x,y) r = and [ elem (x,v) r | (u,v) <- r, u == y ]

equivalenceR :: Eq a => [a] -> [(a,a)] -> Bool
equivalenceR xs r = reflR xs r && symR r && transR r

isS5 :: (Eq a) => [a] -> [(Agent,a,a)] -> Bool
isS5 xs triples =
  all (equivalenceR xs) rels
    where rels = [ accFor i triples | i <- all_agents ]

pairs2rel :: (Eq a, Eq b) => [(a,b)] -> a -> b -> Bool
pairs2rel pairs = \ x y -> elem (x,y) pairs

rel2part :: (Eq a) => [a] -> (a -> a -> Bool) -> [[a]]
rel2part [] r = []
rel2part (x:xs) r = xblock : rel2part rest r
  where
  (xblock,rest) = partition (\ y -> r x y) (x:xs)

equiv2part :: Eq a => [a] -> [(a,a)] -> [[a]]
equiv2part xs r = rel2part xs (pairs2rel r)

euclideanR :: Eq a => [(a,a)] -> Bool
euclideanR s = and [ eucl pair s | pair <- s ]
  where
  eucl (x,y) r = and [ elem (y,v) r | (u,v) <- r, u == x ]

serialR :: Eq a => [a] -> [(a,a)] -> Bool
serialR [] s = True
serialR (x:xs) s = any (\ p -> (fst p) == x) s && serialR xs s

kd45R :: Eq a => [a] -> [(a,a)] -> Bool
kd45R xs r = transR r && serialR xs r && euclideanR r

k45R :: Eq a => [(a,a)] -> Bool
k45R r = transR r && euclideanR r

isolated  :: Eq a =>  [(a,a)] -> a -> Bool
isolated r x = notElem x (map fst r ++ map snd r)

k45PointsBalloons :: Eq a => [a] -> [(a,a)] -> Maybe ([a],[([a],[a])])
k45PointsBalloons xs r =
  let
     orphans = filter (isolated r) xs
     ys = xs \\ orphans
  in
    case kd45Balloons ys r of
      Just balloons -> Just (orphans,balloons)
      Nothing       -> Nothing

entryPair :: Eq a => [(a,a)] -> (a,a) -> Bool
entryPair r = \ (x,y) -> notElem (y,x) r

kd45Balloons :: Eq a => [a] -> [(a,a)] -> Maybe [([a],[a])]
kd45Balloons xs r =
  let
     (s,t)          = partition (entryPair r) r
     entryPoints    = map fst s
     nonentryPoints = xs \\ entryPoints
     s5part xs r = if equivalenceR xs r
                      then Just (equiv2part xs t)
                      else Nothing
  in
    case s5part nonentryPoints t of
      Just part ->
        Just [ (nub (map fst (filter (\ (x,y) -> elem y block) s)),
                block) |     block <- part                            ]
      Nothing   ->
        Nothing

k45 :: (Eq a, Ord a) => [a] ->
    [(Agent,a,a)] -> Maybe [(Agent,([a],[([a],[a])]))]
k45 xs triples =
  if and [ maybe False (\ x -> True) b | (a,b) <- results  ]
     then Just [ (a, maybe undefined id b) | (a,b) <- results  ]
     else Nothing
       where rels     = [ (a, accFor a triples)  | a     <- all_agents ]
             results  = [ (a, k45PointsBalloons xs r) | (a,r) <- rels   ]

kd45 :: (Eq a, Ord a) => [a] -> [(Agent,a,a)] -> Maybe [(Agent,[([a],[a])])]
kd45 xs triples =
  if and [ maybe False (\ x -> True) b | (a,b) <- balloons ]
     then Just [ (a, maybe undefined id b) | (a,b) <- balloons ]
     else Nothing
       where rels     = [ (a, accFor a triples)  | a     <- all_agents ]
             balloons = [ (a, kd45Balloons xs r) | (a,r) <- rels       ]

kd45psbs2balloons :: (Eq a, Ord a) =>
  [(Agent,([a],[([a],[a])]))] -> Maybe [(Agent,[([a],[a])])]
kd45psbs2balloons psbs =
  if all (\ x -> x == []) entryList
     then Just balloons
     else Nothing
  where
    entryList  = [ fst bs      | (a,bs) <- psbs ]
    balloons   = [ (a, snd bs) | (a,bs) <- psbs ]

s5ball2part :: (Eq a, Ord a) =>
  [(Agent,[([a],[a])])] -> Maybe [(Agent,[[a]])]
s5ball2part balloons =
  if all (\ x -> x == []) entryList
     then Just partitions
     else Nothing
  where
    entryList  = [ concat (map fst bs) | (a,bs) <- balloons ]
    partitions = [ (a, map snd bs)     | (a,bs) <- balloons ]

display :: Show a => Int -> [a] -> IO()
display n = if n < 1 then error "parameter not positive"
                     else display' n n
  where
  display' ::  Show a => Int -> Int -> [a] -> IO()
  display' n m [] = putChar '\n'
  display' n 1 (x:xs) =   do (putStr . show) x
                             putChar '\n'
                             display' n n xs
  display' n m (x:xs) =   do (putStr . show) x
                             display' n (m-1) xs

showMo :: (Eq state, Show state, Ord state, Show formula) =>
                Model state formula -> IO()
showMo = displayM 10

showM :: (Eq state, Show state, Ord state, Show formula) =>
                               Pmod state formula -> IO()
showM (Pmod sts pre acc pnts) = do putStr "==> "
                                   print pnts
                                   showMo (Mo sts pre acc)

showMs :: (Eq state, Show state, Ord state, Show formula) =>
                               [Pmod state formula] -> IO()
showMs ms = sequence_ (map showM ms)

displayM :: (Eq state, Show state, Ord state, Show formula) =>
                Int -> Model state formula -> IO()
displayM n (Mo states pre rel) =
  do print states
     display (div n 2) pre
     case (k45 states rel) of
       Nothing       -> display n rel
       Just psbs     -> case kd45psbs2balloons psbs of
         Nothing       -> displayPB (div n 2) psbs
         Just balloons -> case s5ball2part balloons of
           Nothing          ->  displayB (div n 2) balloons
           Just parts       ->  displayP (2*n) parts

displayP :: Show a => Int -> [(Agent,[[a]])] -> IO()
displayP n parts = sequence_ (map (display n) (map (\x -> [x]) parts))

displayB :: Show a => Int -> [(Agent,[([a],[a])])] -> IO()
displayB n balloons = sequence_ (map (display n) (map (\x -> [x]) balloons))

displayPB :: Show a => Int ->  [(Agent,([a],[([a],[a])]))] -> IO()
displayPB n  psbs = sequence_ (map (display n) (map (\x -> [x]) psbs))

class Show a => GraphViz a where
  graphviz :: a -> String

glueWith :: String -> [String] -> String
glueWith _ []     = []
glueWith _ [y]    = y
glueWith s (y:ys) = y ++ s ++ glueWith s ys

listState  :: (Show a, Show b, Eq a, Eq b) => a -> [(a,b)] -> String
listState w val =
    let
      props = head (maybe [] (\ x -> [x]) (lookup w val))
      label = filter (isAlphaNum) (show props)
    in
      if null label
         then show w
         else show w
              ++ "[label =\"" ++ (show w) ++ ":" ++ (show props) ++ "\"]"

links :: (Eq a, Eq b) => [(a,b,b)] -> [(a,b,b)]
links [] = []
links ((x,y,z):xyzs) | y == z    = links xyzs
                     | otherwise =
                       (x,y,z): links (filter (/= (x,z,y)) xyzs)

cmpl :: Eq b => [(Agent,b,b)] -> [([Agent],b,b)]
cmpl [] = []
cmpl ((x,y,z):xyzs) = (xs,y,z):(cmpl xyzs')
  where xs = x: [ a | a  <- all_agents, elem a (map f xyzs1) ]
        xyzs1 = filter (\ (u,v,w) ->
                       (v == y && w == z)
                               ||
                       (v == z && w == y)) xyzs
        f (x,_,_) = x
        xyzs' = xyzs \\ xyzs1

instance (Show a, Show b, Eq a, Eq b) => GraphViz (Model a b) where
  graphviz (Mo states val rel) = if isS5 states rel
   then
    "digraph G { "
      ++
      glueWith  " ; " [ listState s val | s <- states ]
      ++  " ; " ++
      glueWith " ; " [ (show s) ++ " -> " ++ (show s')
                                ++ " [label="
                                ++ (filter isAlpha (show ags))
                                ++ ",dir=none ]"  |
                         s <- states, s' <- states,
                         (ags,t,t') <- (cmpl . links) rel,
                         s == t, s' == t'                         ]
      ++ " }"
   else
    "digraph G { "
      ++
      glueWith  " ; " [ listState s val | s <- states ]
      ++  " ; " ++
      glueWith " ; " [ (show s) ++ " -> " ++ (show s')
                                ++ " [label=" ++ (show ag) ++ "]"  |
                         s <- states, s' <- states,
                         (ag,t,t') <- rel,
                         s == t, s' == t'                         ]
      ++ " }"

listPState  :: (Show a, Show b, Eq a, Eq b) =>
                a -> [(a,b)] -> Bool -> String
listPState w val pointed =
    let
      props = head (maybe [] (\ x -> [x]) (lookup w val))
      label = filter (isAlphaNum) (show props)
    in
      if null label
         then if pointed then show w ++ "[peripheries = 2]"
              else            show w
         else if pointed then
              show w
              ++ "[label =\"" ++ (show w) ++ ":" ++ (show props) ++
                 "\",peripheries = 2]"
              else show w
              ++ "[label =\"" ++ (show w) ++ ":" ++ (show props) ++ "\"]"

instance (Show a, Show b, Eq a, Eq b) => GraphViz (Pmod a b)  where
  graphviz (Pmod states val rel points) = if isS5 states rel
   then
    "digraph G { "
      ++
      glueWith  " ; " [ listPState s val (elem s points) | s <- states ]
      ++  " ; " ++
      glueWith " ; " [ (show s) ++ " -> " ++ (show s')
                                ++ " [label="
                                ++ (filter isAlpha (show ags))
                                ++  ",dir=none ]"  |
                         s <- states, s' <- states,
                         (ags,t,t') <- (cmpl . links) rel,
                         s == t, s' == t'                         ]
      ++ " }"
   else
    "digraph G { "
      ++
      glueWith  " ; " [ listPState s val (elem s points) | s <- states ]
      ++  " ; " ++
      glueWith " ; " [ (show s) ++ " -> " ++ (show s')
                                ++ " [label=" ++ (show ag) ++ "]"  |
                         s <- states, s' <- states,
                         (ag,t,t') <- rel,
                         s == t, s' == t'                         ]
      ++ " }"

writeGraph :: String -> IO()
writeGraph cts = writeFile "graph.dot" cts

writeGr :: String -> String -> IO()
writeGr name cts = writeFile name cts

writeModel :: (Show a, Show b, Eq a, Eq b) => Model a b -> IO()
writeModel m = writeGraph (graphviz m)

writePmod :: (Show a, Show b, Eq a, Eq b) => (Pmod a b) -> IO()
writePmod m = writeGraph (graphviz m)

writeP :: (Show a, Show b, Eq a, Eq b) => String -> (Pmod a b) -> IO()
writeP name m = writeGr (name ++ ".dot") (graphviz m)

mapping :: Form -> [(Form,Integer)]
mapping f = zip lits [1..k]
  where
  lits = (sort . nub . collect) f
  k    = toInteger (length lits)
  collect :: Form -> [Form]
  collect Top         = []
  collect (Prop p)    = [Prop p]
  collect (Neg f)     = collect f
  collect (Conj fs)   = concat (map collect fs)
  collect (Disj fs)   = concat (map collect fs)
  collect (Pr pr f)   = if canonF f == Top then [] else [Pr pr (canonF f)]
  collect (K ag f)    = if canonF f == Top then [] else [K ag (canonF f)]
  collect (EK ags f)  = if canonF f == Top then [] else [EK ags (canonF f)]
  collect (CK ags f)  = if canonF f == Top then [] else [CK ags (canonF f)]
  collect (Up pam f)  = if canonF f == Top then [] else [Up pam (canonF f)]
  collect (Aut nfa f) = if nfa == nullAut || canonF f == Top
                        then [] else [Aut nfa (canonF f)]

cf :: (Form -> Integer) -> Form -> [[Integer]]
cf g (Top)            = []
cf g (Prop p)         = [[g (Prop p)]]
cf g (Pr pr f)        = if canonF f == Top then []
                        else [[g (Pr pr (canonF f))]]
cf g (K ag f)         = if canonF f == Top then []
                        else [[g (K ag (canonF f))]]
cf g (EK ags f)       = if canonF f == Top then []
                        else [[g (EK ags (canonF f))]]
cf g (CK ags f)       = if canonF f == Top then []
                        else [[g (CK ags (canonF f))]]
cf g (Up am f)        = if canonF f == Top then []
                        else [[g (Up am (canonF f))]]
cf g (Aut nfa f)      = if nfa == nullAut || canonF f == Top then []
                        else [[g (Aut nfa (canonF f))]]
cf g (Conj fs)        = concat (map (cf g) fs)
cf g (Disj fs)        = deMorgan (map (cf g) fs)

cf g (Neg Top)        = [[]]
cf g (Neg (Prop p))   = [[- g (Prop p)]]
cf g (Neg (Pr pr f))  = if canonF f == Top then [[]]
                        else [[- g (Pr pr (canonF f))]]
cf g (Neg (K ag f))   = if canonF f == Top then [[]]
                        else [[- g (K ag (canonF f))]]
cf g (Neg (EK ags f)) = if canonF f == Top then [[]]
                        else [[- g (EK ags (canonF f))]]
cf g (Neg (CK ags f)) = if canonF f == Top then [[]]
                        else [[- g (CK ags (canonF f))]]
cf g (Neg (Up am f))  = if canonF f == Top then [[]]
                        else [[- g (Up am (canonF f))]]
cf g (Neg (Aut nfa f))= if nfa == nullAut || canonF f == Top then [[]]
                        else [[- g (Aut nfa (canonF f))]]
cf g (Neg (Conj fs))  = deMorgan (map (\ f -> cf g (Neg f)) fs)
cf g (Neg (Disj fs))  = concat   (map (\ f -> cf g (Neg f)) fs)
cf g (Neg (Neg f))    = cf g f

deMorgan :: [[[Integer]]] -> [[Integer]]
deMorgan [] = [[]]
deMorgan [cls] = cls
deMorgan (cls:clss) = deMorg cls (deMorgan clss)
  where
  deMorg :: [[Integer]] -> [[Integer]] -> [[Integer]]
  deMorg cls1 cls2 = (nub . concat) [ deM cl cls2 | cl <- cls1 ]
  deM :: [Integer] -> [[Integer]]  -> [[Integer]]
  deM cl cls = map (fuseLists cl) cls

fuseLists :: [Integer] -> [Integer] -> [Integer]
fuseLists [] ys = ys
fuseLists xs [] = xs
fuseLists (x:xs) (y:ys) | abs x < abs y  = x:(fuseLists xs (y:ys))
                        | abs x == abs y = if x == y
                                              then x:(fuseLists xs ys)
                                              else if x > y
                                                then x:y:(fuseLists xs ys)
                                                else y:x:(fuseLists xs ys)
                        | abs x > abs y  = y:(fuseLists (x:xs) ys)

satVals :: [(Form,Integer)] -> Form -> [[Integer]]
satVals t f = (map fst . dp) (cf (table2fct t) f)

propEquiv :: Form -> Form -> Bool
propEquiv f1 f2 = satVals g f1 == satVals g f2
  where g = mapping (Conj [f1,f2])

contrad :: Form -> Bool
contrad f = propEquiv f (Disj [])

consistent :: Form -> Bool
consistent = not . contrad

canonF :: Form -> Form
canonF f = if (contrad (Neg f))
             then Top
             else if fs == []
             then Neg Top
             else if length fs == 1
             then head fs
             else Disj fs
  where g   = mapping f
        nss = satVals g f
        g'  = \ i -> head [ form | (form,j) <- g, i == j ]
        h   = \ i -> if i < 0 then Neg (g' (abs i)) else g' i
        h'  = \ xs -> map h xs
        k   = \ xs -> if xs == []
                         then Top
                         else if length xs == 1
                                 then head xs
                                 else Conj xs
        fs  = map k (map h' nss)

lookupFs :: (Eq a,Eq b) => a -> a -> [(a,b)] -> (b -> b -> Bool) -> Bool
lookupFs i j table r = case lookup i table of
  Nothing -> lookup j table == Nothing
  Just f1 -> case lookup j table of
      Nothing -> False
      Just f2 -> r f1 f2

initPartition :: (Eq a, Eq b) => Model a b -> (b -> b -> Bool) -> [[a]]
initPartition (Mo states pre rel) r =
  rel2part states (\ x y -> lookupFs x y pre r)

refinePartition :: (Eq a, Eq b) => Model a b -> [[a]] -> [[a]]
refinePartition m p = refineP m p p
  where
  refineP :: (Eq a, Eq b) => Model a b -> [[a]] -> [[a]] -> [[a]]
  refineP m part [] = []
  refineP m@(Mo states pre rel) part (block:blocks) =
     newblocks ++ (refineP m part blocks)
       where
         newblocks =
           rel2part block (\ x y -> sameAccBlocks m part x y)

sameAccBlocks :: (Eq a, Eq b) =>
         Model a b -> [[a]] -> a -> a -> Bool
sameAccBlocks m@(Mo states pre rel) part s t =
    and [ accBlocks m part s ag == accBlocks m part t  ag |
                                               ag <- all_agents ]

accBlocks :: (Eq a, Eq b) => Model a b -> [[a]] -> a -> Agent -> [[a]]
accBlocks m@(Mo states pre rel) part s ag =
    nub [ bl part y | (ag',x,y) <- rel, ag' == ag, x == s ]

bl :: (Eq a) => [[a]] -> a -> [a]
bl part x = head (filter (\ b -> elem x b) part)

initRefine :: (Eq a, Eq b) => Model a b -> (b -> b -> Bool) -> [[a]]
initRefine m r = refine m (initPartition m r)

refine :: (Eq a, Eq b) => Model a b -> [[a]] -> [[a]]
refine m part = if rpart == part
                       then part
                       else refine m rpart
  where rpart = refinePartition m part

minimalModel :: (Eq a, Ord a, Eq b, Ord b) =>
                 (b -> b -> Bool) -> Model a b -> Model [a] b
minimalModel r m@(Mo states pre rel) =
  (Mo states' pre' rel')
     where
     partition = initRefine m r
     states'   = partition
     f         = bl partition
     rel'      = (nub.sort) (map (\ (x,y,z) -> (x, f y, f z)) rel)
     pre'      = (nub.sort) (map (\ (x,y)   -> (f x, y))      pre)

minimalPmod :: (Eq a, Ord a, Eq b, Ord b) =>
                  (b -> b -> Bool) -> Pmod a b -> Pmod [a] b
minimalPmod r (Pmod sts pre rel pts) = (Pmod sts' pre' rel' pts')
  where (Mo sts' pre' rel') = minimalModel r (Mo sts pre rel)
        pts' = map (bl sts') pts

convert :: (Eq a, Show a) => [a] -> a  -> Integer
convert = convrt 0
  where
  convrt :: (Eq a, Show a) => Integer -> [a] -> a -> Integer
  convrt n []     x = error (show x ++ " not in list")
  convrt n (y:ys) x | x == y    = n
                    | otherwise = convrt (n+1) ys x

conv ::  (Eq a, Show a) => Model a b -> Model State b
conv  (Mo worlds val acc) =
      (Mo (map f worlds)
          (map (\ (x,y)   -> (f x, y)) val)
          (map (\ (x,y,z) -> (x, f y, f z)) acc))
  where f = convert worlds

convPmod ::  (Eq a, Show a) => Pmod a b -> Pmod State b
convPmod (Pmod sts pre rel pts) = (Pmod sts' pre' rel' pts')
   where (Mo sts' pre' rel') = conv (Mo sts pre rel)
         pts' = nub (map (convert sts) pts)

bisim ::  (Eq a, Ord a, Show a, Eq b, Ord b) =>
           (b -> b -> Bool) -> Model a b -> Model State b
bisim r = conv . (minimalModel r)

bisimPmod ::  (Eq a, Ord a, Show a, Eq b, Ord b) =>
            (b -> b -> Bool) -> Pmod a b -> Pmod State b
bisimPmod r = convPmod . (minimalPmod r)

unfold :: PoAM -> PoAM
unfold (Pmod states pre acc [])     = zero
unfold am@(Pmod states pre acc [p]) = am
unfold (Pmod states pre acc points) =
  Pmod states' pre' acc' points'
  where
  len = toInteger (length states)
  points' = [ p + len | p <- points ]
  states' = states ++ points'
  pre'    = pre ++ [ (j+len,f) | (j,f) <- pre, k <- points, j == k ]
  acc'    = acc ++ [ (ag,i+len,j) | (ag,i,j) <- acc, k <- points, i == k ]

preds, sucs :: (Eq a, Ord a, Eq b, Ord b) => [(a,b,b)] -> b -> [(a,b)]
preds rel s = (sort.nub) [ (ag,s1) | (ag,s1,s2) <- rel, s == s2 ]
sucs  rel s = (sort.nub) [ (ag,s2) | (ag,s1,s2) <- rel, s == s1 ]

psPartition :: (Eq a, Ord a, Eq b) => Model a b -> [[a]]
psPartition (Mo states pre rel) =
  rel2part states (\ x y -> preds rel x == preds rel y
                            &&
                            sucs rel x == sucs rel y)

minPmod :: (Eq a, Ord a) => Pmod a Form -> Pmod [a] Form
minPmod pm@(Pmod states pre rel pts) =
  (Pmod states' pre' rel' pts')
     where
     m         = Mo states pre rel
     partition = refine m (psPartition m)
     states'   = partition
     f         = bl partition
     g         = \ xs -> canonF (Disj (map (table2fct pre) xs))
     rel'      = (nub.sort) (map (\ (x,y,z) -> (x, f y, f z)) rel)
     pre'      = zip states' (map g states')
     pts'      = map (bl states') pts

aePmod ::  (Eq a, Ord a, Show a) => Pmod a Form -> Pmod State Form
--aePmod ::  PoAM -> PoAM
aePmod = (bisimPmod propEquiv) . minPmod . unfold .
                          (bisimPmod propEquiv) . gsmPoAM . convPmod

am0 = ndSum' (test p) (test (Neg p))

am1 = ndSum' (test p) (ndSum' (test q) (test r))

transf :: PoAM -> Integer -> Integer -> Program -> Program
transf am@(Pmod states pre acc points) i j (Ag ag) =
   let
     f = table2fct pre i
   in
   if elem (ag,i,j) acc && f == Top          then Ag ag
   else if elem (ag,i,j) acc && f /= Neg Top then Conc [Test f, Ag ag]
   else Test (Neg Top)
transf am@(Pmod states pre acc points) i j (Ags ags) =
   let ags' = nub [ a | (a,k,m) <- acc, elem a ags, k == i, m == j ]
       ags1 = intersect ags ags'
       f    = table2fct pre i
   in
     if ags1 == [] || f == Neg Top        then Test (Neg Top)
     else if f == Top && length ags1 == 1 then Ag (head ags1)
     else if f == Top                     then Ags ags1
     else Conc [Test f, Ags ags1]
transf am@(Pmod states pre acc points) i j (Test f) =
   let
     g = table2fct pre i
   in
   if i == j
      then Test (Conj [g,(Up am f)])
      else Test (Neg Top)
transf am@(Pmod states pre acc points) i j (Conc [])  =
  transf am i j (Test Top)
transf am@(Pmod states pre acc points) i j (Conc [p]) = transf am i j p
transf am@(Pmod states pre acc points) i j (Conc (p:ps)) =
  Sum [ Conc [transf am i k p, transf am k j (Conc ps)] | k <- [0..n] ]
    where n = toInteger (length states - 1)
transf am@(Pmod states pre acc points) i j (Sum [])  =
  transf am i j (Test (Neg Top))
transf am@(Pmod states pre acc points) i j (Sum [p]) = transf am i j p
transf am@(Pmod states pre acc points) i j (Sum ps)  =
  Sum [ transf am i j p | p <- ps ]
transf am@(Pmod states pre acc points) i j (Star p) = kleene am i j n p
  where n = toInteger (length states)

kleene ::  PoAM -> Integer -> Integer -> Integer -> Program -> Program
kleene am i j 0 pr =
  if i == j
    then Sum [Test Top, transf am i j pr]
    else transf am i j pr
kleene am i j k pr
  | i == j && j == pred k = Star (kleene am i i i pr)
  | i == pred k           =
    Conc [Star (kleene am i i i pr), kleene am i j i pr]
  | j == pred k           =
    Conc [kleene am i j j pr, Star (kleene am j j j pr)]
  | otherwise             =
      Sum [kleene am i j k' pr,
           Conc [kleene am i k' k' pr,
                 Star (kleene am k' k' k' pr), kleene am k' j k' pr]]
      where k' = pred k

tfm ::  PoAM -> Integer -> Integer -> Program -> Program
tfm am i j pr = simpl (transf am i j pr)

step0, step1 :: PoAM -> Program -> Form -> Form
step0 am@(Pmod states pre acc []) pr f = Top
step0 am@(Pmod states pre acc [i]) pr f = step1 am pr f
step0 am@(Pmod states pre acc is) pr f =
  Conj [ step1 (Pmod states pre acc [i]) pr f | i <- is ]
step1 am@(Pmod states pre acc [i]) pr f =
   Conj [ Pr (transf am i j (rpr pr))
              (Up (Pmod states pre acc [j]) f) | j <- states ]

step :: PoAM -> Program -> Form -> Form
step am pr f = canonF (step0 am pr f)

t :: Form -> Form
t Top = Top
t (Prop p) = Prop p
t (Neg f) = Neg (t f)
t (Conj fs) = Conj (map t fs)
t (Disj fs) = Disj (map t fs)
t (Pr pr f) = Pr (rpr pr) (t f)
t (K x f)   = Pr (Ag x) (t f)
t (EK xs f)  = Pr (Ags xs) (t f)
t (CK xs f)  = Pr (Star (Ags xs)) (t f)

t (Up am@(Pmod states pre acc [i]) f) = t' am f
t (Up am@(Pmod states pre acc is) f)  =
   Conj [ t' (Pmod states pre acc [i]) f | i <- is ]

t' :: PoAM -> Form -> Form
t' am Top            = Top
t' am (Prop p)       = impl (precondition am) (Prop p)
t' am (Neg f)        =  Neg (t' am f)
t' am (Conj fs)      = Conj (map (t' am) fs)
t' am (Disj fs)      = Disj (map (t' am) fs)
t' am (K x f)        = t' am (Pr (Ag x) f)
t' am (EK xs f)      = t' am (Pr (Ags xs) f)
t' am (CK xs f)      = t' am (Pr (Star (Ags xs)) f)
t' am (Up am' f)     = t' am (t (Up am' f))

t' am@(Pmod states pre acc [i]) (Pr pr f) =
   Conj [ Pr (transf am i j (rpr pr))
              (t' (Pmod states pre acc [j]) f) | j <- states ]
t' am@(Pmod states pre acc is) (Pr pr f) =
   error "action model not single pointed"

rpr :: Program -> Program
rpr (Ag x)       = Ag x
rpr (Ags xs)     = Ags xs
rpr (Test f)     = Test (t f)
rpr (Conc ps)    = Conc (map rpr ps)
rpr (Sum  ps)    = Sum  (map rpr ps)
rpr (Star p)     = Star (rpr p)

tr :: Form -> Form
tr = canonF . t

data Symbol = Acc Agent | Tst Form deriving (Eq,Ord,Show)

data (Eq a,Ord a,Show a) => Move a = Move a Symbol a deriving (Eq,Ord,Show)

data (Eq a,Ord a,Show a) => NFA a = NFA a [Move a] a deriving (Eq,Ord,Show)

states :: (Eq a,Ord a,Show a) => NFA a -> [a]
states (NFA s delta f) = (sort . nub) (s:f:rest)
  where rest = [ s' | Move s' a t' <- delta ]
                 ++
               [ t' | Move s' a t' <- delta ]

symbols :: (Eq a,Ord a,Show a) => NFA a -> [Symbol]
symbols (NFA s moves f) = (sort . nub) [ symb | Move s symb t <- moves ]

recog :: (Eq a,Ord a,Show a) => NFA a -> [Symbol] -> Bool
recog (NFA start moves final) [] = start == final
recog (NFA start moves final) (symbol:symbols) =
  any (\ aut -> recog aut symbols)
     [ NFA new moves final |
           Move s symb new <- moves, s == start, symb == symbol ]

reachable :: (Eq a,Ord a,Show a) => NFA a -> [a]
reachable (NFA start moves final) = acc moves [start] []
  where
  acc :: (Show a,Ord a) => [Move a] -> [a] -> [a] -> [a]
  acc moves [] marked = marked
  acc moves (b:bs) marked = acc moves (bs ++ (cs \\ bs)) (marked ++ cs)
     where
     cs  = nub [ c | Move b' symb c <- moves, b' == b, notElem c marked ]

accNFA :: (Eq a,Ord a,Show a) => NFA a -> NFA a
accNFA nfa@(NFA start moves final) =
  if
    notElem final fromStart
  then
    NFA start [] final
  else
    NFA start moves' final
  where
   fromStart = reachable nfa
   moves' = [ Move x symb y | Move x symb y <- moves, elem x fromStart ]

initPart :: (Eq a,Ord a,Show a) => NFA a -> [[a]]
initPart nfa@(NFA start moves final) = [states nfa \\ [final], [final]]

refinePart :: (Eq a, Ord a, Show a) => NFA a -> [[a]] -> [[a]]
refinePart nfa p = refineP nfa p p
  where
  refineP :: (Eq a, Ord a, Show a) => NFA a -> [[a]] -> [[a]] -> [[a]]
  refineP nfa part [] = []
  refineP nfa@(NFA start moves final) part (block:blocks) =
     newblocks ++ (refineP nfa part blocks)
       where
         newblocks =
           rel2part block (\ x y -> sameAccBl nfa part x y)

sameAccBl :: (Eq a, Ord a, Show a) => NFA a -> [[a]] -> a -> a -> Bool
sameAccBl nfa part s t =
    and [ accBl nfa part s symb == accBl nfa part t symb |
                                               symb <- symbols nfa ]

accBl :: (Eq a, Ord a, Show a) => NFA a -> [[a]] -> a -> Symbol -> [[a]]
accBl nfa@(NFA start moves final) part s symb =
   nub [ bl part y | Move x symb' y <- moves, symb' == symb, x == s ]

compress :: (Eq a, Ord a, Show a) => NFA a -> [[a]]
compress nfa = compress' nfa (initPart nfa)
  where
  compress' :: (Eq a, Ord a, Show a) => NFA a -> [[a]] -> [[a]]
  compress' nfa part = if rpart == part
                         then part
                         else compress' nfa rpart
   where rpart = refinePart nfa part

minimalAut' :: (Eq a, Ord a, Show a) => NFA a -> NFA [a]
minimalAut' nfa@(NFA start moves final) = NFA start' moves' final'
  where
   (NFA st mov fin) = accNFA nfa
   partition    = compress (NFA st mov fin)
   f            = bl partition
   g (Acc ag)   = Acc ag
   g (Tst frm) = Tst (canonF frm)
   start'       = f st
   final'       = f fin
   moves'       = (nub.sort)
                     (map (\ (Move x y z) -> Move (f x) (g y) (f z)) mov)

convAut :: (Eq a,Ord a,Show a) => NFA a -> NFA State
convAut aut@(NFA s delta t) =
    NFA
    (f s)
    (map (\ (Move x symb y) -> Move (f x) symb (f y)) delta)
    (f t)
    where f = convert (states aut)

minimalAut :: (Eq a, Ord a, Show a) => NFA a -> NFA State
minimalAut = convAut . minimalAut'

nullAut = (NFA 0 [] 1)

genKnown :: [Agent] -> NFA State
genKnown agents = (NFA 0 [Move 0 (Acc a) 1 | a <- agents ] 1)

relCknown :: [Agent] -> Form -> NFA State
relCknown agents form = (NFA 0 (Move 0 (Tst form) 1 :
                               [Move 1 (Acc a) 0 | a <- agents]) 0)

cKnown :: [Agent] -> NFA State
cKnown agents = (NFA 0 [Move 0 (Acc a) 0 | a <- agents] 0)

aut' :: (Show a,Ord a) =>
            PoAM -> State -> State -> NFA a -> NFA (State,Int,a)
aut' (Pmod sts pre acc _) s t (NFA start delta final) =
  (NFA (s,0,start) delta' (t,1,final)) where
    delta' = [ Move (u,1,w) (Acc a) (v,0,x) |
                (a,u,v) <- acc,
                Move w (Acc a') x <- delta,
                 a == a' ]
               ++
             [ Move (u,0,w) (Tst (table2fct pre u)) (u,1,w) |
                 u <- sts,
                 w <- states (NFA start delta final) ]
               ++
             [ Move (u,1,v)
                (Tst (Neg (Up (Pmod sts pre acc [u])
                                      (Neg form)))) (u,1,w) |
               u <- sts,
               Move v (Tst form) w <- delta ]

aut :: (Show a,Ord a) => PoAM -> State -> State -> NFA a -> NFA State
aut am s t nfa = minimalAut (aut' am s t nfa)

tr' :: Form -> Form
tr' Top = Top
tr' (Prop p) = Prop p
tr' (Neg form) = Neg (tr' form)
tr' (Conj forms) = Conj (map tr' forms)
tr' (Disj forms) = Disj (map tr' forms)
tr' (K agent form) = K agent (tr' form)
tr' (EK agents form) = Aut (genKnown agents) (tr' form)
tr' (CK agents form) = Aut (cKnown agents) (tr' form)

tr' (Aut nfa form) = Aut (tAut nfa) (tr' form)
tr' (Up (Pmod sts pre rel []) form) = Top
tr' (Up (Pmod sts pre rel [s]) Top) = Top
tr' (Up (Pmod sts pre rel [s]) (Prop p)) =
  impl (tr' (table2fct pre s)) (Prop p)
tr' (Up (Pmod sts pre rel [s]) (Neg form)) =
  impl (tr' (table2fct pre s))
    (Neg (tr' (Up (Pmod sts pre rel [s]) form)))
tr' (Up (Pmod sts pre rel [s]) (Conj forms)) =
  Conj [ tr' (Up (Pmod sts pre rel [s]) form) | form <- forms ]
tr' (Up (Pmod sts pre rel [s]) (Disj forms)) =
  Disj [ tr' (Up (Pmod sts pre rel [s]) form) | form <- forms ]
tr' (Up (Pmod sts pre rel [s]) (K agent form)) =
  impl (tr' (table2fct pre s))
    (Conj [ K agent (tr' (Up (Pmod sts pre rel [t]) form)) |
             t <- sts ])
tr' (Up (Pmod sts pre rel [s]) (EK agents form)) =
  tr' (Up (Pmod sts pre rel [s]) (Aut (genKnown agents) form))
tr' (Up (Pmod sts pre rel [s]) (CK agents form)) =
  tr' (Up (Pmod sts pre rel [s]) (Aut (cKnown agents) form))

tr' (Up (Pmod sts pre rel [s]) (Aut nfa form)) =
  Conj [ tr' (Aut (aut (Pmod sts pre rel [s]) s t  nfa)
              (Up  (Pmod sts pre rel [t]) form)) |  t <- sts ]
tr' (Up (Pmod sts pre rel [s]) (Up (Pmod sts' pre' rel' points) form)) =
  tr' (Up (Pmod sts pre rel [s])
    (tr' (Up (Pmod sts' pre' rel' points) form)))
tr' (Up (Pmod sts pre rel points) form) =
  Conj [ tr' (Up (Pmod sts pre rel [s]) form) | s <- points ]

kvbtr :: Form -> Form
kvbtr = canonF . tr'

tAut :: NFA State -> NFA State
tAut (NFA s delta f) = NFA s (map trans delta) f
 where trans (Move u (Acc x) v)     = Move u (Acc x) v
       trans (Move u (Tst form) v) = Move u (Tst (kvbtr form)) v

groupAlts :: [(Agent,State,State)] -> [Agent] -> State -> [State]
groupAlts rel agents current =
  (nub . sort . concat) [ alternatives rel a current | a <- agents ]

commonAlts :: [(Agent,State,State)] -> [Agent] -> State -> [State]
commonAlts rel agents current =
  closure rel agents (groupAlts rel agents current)

up :: EpistM -> PoAM -> Pmod (State,State) [Prop]
up  m@(Pmod worlds val acc points) am@(Pmod states pre susp actuals) =
   Pmod worlds' val' acc' points'
   where
   worlds' = [ (w,s) | w <- worlds, s <- states,
                       formula <- maybe [] (\ x -> [x]) (lookup s pre),
                       isTrAt w m formula                ]
   val'    = [ ((w,s),props) | (w,props) <- val,
                                s        <- states,
                                elem (w,s) worlds'             ]
   acc'    = [ (ag1,(w1,s1),(w2,s2)) | (ag1,w1,w2) <- acc,
                                       (ag2,s1,s2) <- susp,
                                        ag1 == ag2,
                                        elem (w1,s1) worlds',
                                        elem (w2,s2) worlds'   ]
   points' = [ (p,a) | p <- points, a <- actuals,
                       elem (p,a) worlds'                      ]

sameVal :: [Prop] -> [Prop] -> Bool
sameVal ps qs = (nub . sort) ps ==  (nub . sort) qs

upd ::  EpistM -> PoAM -> EpistM
upd sm am = (bisimPmod (sameVal) . convPmod) (up sm am)

upds  :: EpistM -> [PoAM] -> EpistM
upds = foldl upd

reachableAut :: SM -> NFA State -> State -> [State]
reachableAut model nfa@(NFA start moves final) w =
  acc model nfa [(w,start)] []
  where
    acc :: SM -> NFA State -> [(State,State)] -> [(State,State)] -> [State]
    acc model (NFA start moves final) [] marked =
       (sort.nub) (map fst (filter (\ x -> snd x == final) marked))
    acc m@(Mo states _ rel) nfa@(NFA start moves final)
          ((w,s):pairs) marked =
      acc m nfa (pairs ++ (cs \\ pairs)) (marked ++ cs)
      where
        cs = nub ([ (w, s') | Move t (Tst f) s' <- moves,
                              t == s, notElem (w,s') marked,
                              isTrueAt w m f ]
                  ++
                 [ (w',s') | Move t (Acc ag) s' <- moves, t == s,
                             w' <- states,
                             notElem (w',s') marked,
                             elem (ag,w,w') rel ])

isTrueAt :: State -> SM -> Form -> Bool
isTrueAt w m Top = True
isTrueAt w m@(Mo worlds val acc) (Prop p) =
  elem p (concat [ props | (w',props) <- val, w'==w ])
isTrueAt w m (Neg f)   = not (isTrueAt w m f)
isTrueAt w m (Conj fs) = and (map (isTrueAt w m) fs)
isTrueAt w m (Disj fs) = or  (map (isTrueAt w m) fs)

isTrueAt w m@(Mo worlds val acc) (K ag f) =
  and (map (flip ((flip isTrueAt) m) f) (alternatives acc ag w))
isTrueAt w m@(Mo worlds val acc) (EK agents f) =
  and (map (flip ((flip isTrueAt) m) f) (groupAlts acc agents w))
isTrueAt w m@(Mo worlds val acc) (CK agents f) =
  and (map (flip ((flip isTrueAt) m) f) (commonAlts acc agents w))

isTrueAt w m (Up am f) =
  and [ isTrueAt w' m' f |
         (m',w') <- decompose (upd (mod2pmod m [w]) am) ]
isTrueAt w m (Aut nfa f) =
  and [ isTrueAt w' m f | w' <- reachableAut m nfa w ]

isTrAt :: State -> EpistM -> Form -> Bool
isTrAt w (Pmod worlds val rel pts) = isTrueAt w (Mo worlds val rel)

isTrue :: EpistM -> Form -> Bool
isTrue (Pmod worlds val rel pts) form =
   and [ isTrueAt w (Mo worlds val rel) form | w <- pts ]

initE :: [Prop] -> EpistM
initE allProps = (Pmod worlds val accs points)
  where
    worlds = [0..(2^k - 1)]
    k      = length allProps
    val    = zip worlds (sortL (powerList allProps))
    accs   = [ (ag,st1,st2) | ag <- all_agents,
                              st1 <- worlds,
                              st2 <- worlds      ]
    points = worlds

powerList  :: [a] -> [[a]]
powerList  [] = [[]]
powerList  (x:xs) = (powerList xs) ++ (map (x:) (powerList xs))

sortL :: Ord a => [[a]] -> [[a]]
sortL  = sortBy (\ xs ys -> if length xs < length ys then LT
                               else if length xs > length ys then GT
                               else compare xs ys)

e00 :: EpistM
e00 = initE [P 0]

e0 :: EpistM
e0 = initE [P 0,Q 0]

public :: Form -> PoAM
public form =
   (Pmod [0] [(0,form)] [ (a,0,0) | a <- all_agents ] [0])

groupM :: [Agent] -> Form -> PoAM
groupM agents form =
  if (sort agents) == all_agents
    then public form
    else
      (Pmod
         [0,1]
         [(0,form),(1,Top)]
         ([ (a,0,0) | a <- all_agents ]
           ++ [ (a,0,1) | a <- all_agents \\ agents ]
           ++ [ (a,1,0) | a <- all_agents \\ agents ]
           ++ [ (a,1,1) | a <- all_agents           ])
         [0])

message :: Agent -> Form -> PoAM
message agent form = groupM [agent] form

secret :: [Agent] -> Form -> PoAM
secret agents form =
  if (sort agents) == all_agents
    then public form
    else
      (Pmod
         [0,1]
         [(0,form),(1,Top)]
         ([ (a,0,0) | a <- agents ]
           ++ [ (a,0,1) | a <- all_agents \\ agents ]
           ++ [ (a,1,1) | a <- all_agents           ])
         [0])

test :: Form -> PoAM
test = secret []

reveal :: [Agent] -> [Form] -> PoAM
reveal ags forms =
  (Pmod
     states
     (zip states forms)
     ([ (ag,s,s) | s <- states, ag <- ags ]
       ++
      [ (ag,s,s') | s <- states, s' <- states, ag <- others ])
     states)
    where states = map fst (zip [0..] forms)
          others = all_agents \\ ags

negation :: Form -> Form
negation (Neg form) = form
negation form       = Neg form

info :: [Agent] -> Form -> PoAM
info agents form = reveal agents [form, negation form]

one :: PoAM
one = public Top

cmpP :: PoAM -> PoAM ->
                 Pmod (State,State) Form
cmpP m@(Pmod states pre susp ss) (Pmod states' pre' susp' ss') =
  (Pmod nstates npre nsusp npoints)
       where
         npoints = [ (s,s') | s <- ss, s' <- ss' ]
         nstates = [ (s,s') | s <- states, s' <- states' ]
         npre    = [ ((s,s'), g) | (s,f)     <- pre,
                                   (s',f')   <- pre',
                                    g        <- [computePre m f f']      ]
         nsusp   = [ (ag,(s1,s1'),(s2,s2')) | (ag,s1,s2)    <- susp,
                                              (ag',s1',s2') <- susp',
                                               ag == ag'                 ]

computePre  :: PoAM -> Form -> Form -> Form
computePre m g g'  | pureProp conj = conj
                   | otherwise     = Conj [ g, Neg (Up m (Neg g')) ]
  where conj    = canonF (Conj [g,g'])

cmpPoAM :: PoAM -> PoAM -> PoAM
cmpPoAM pm pm' = aePmod (cmpP pm pm')

cmp :: [PoAM] -> PoAM
cmp = foldl cmpPoAM one

m2  = initE [P 0,Q 0]
psi = Disj[Neg(K b p),q]

pow :: Int -> PoAM -> PoAM
pow n am = cmp (take n (repeat am))

vBtest :: EpistM -> PoAM -> [EpistM]
vBtest m a = map (upd m) (star one cmpPoAM a)

star :: a -> (a -> a -> a) -> a -> [a]
star z f a = z : star (f z a) f a

vBfix :: EpistM -> PoAM -> [EpistM]
vBfix m a = fix (vBtest m a)

fix :: Eq a => [a] -> [a]
fix (x:y:zs) = if x == y then [x]
                         else x: fix (y:zs)

m1  = initE [P 1,P 2,P 3]
phi = Conj[p1,Neg (Conj[K a p1,Neg p2]),
              Neg (Conj[K a p2,Neg p3])]
a1  = message a phi

ndSum' :: PoAM -> PoAM -> PoAM
ndSum' m1 m2 = (Pmod states val acc ss)
  where
       (Pmod states1 val1 acc1 ss1) = convPmod m1
       (Pmod states2 val2 acc2 ss2) = convPmod m2
       f   = \ x -> toInteger (length states1) + x
       states2' = map f states2
       val2'    = map (\ (x,y)   -> (f x, y)) val2
       acc2'    = map (\ (x,y,z) -> (x, f y, f z)) acc2
       ss       = ss1 ++ map f ss2
       states   = states1 ++ states2'
       val      = val1 ++ val2'
       acc      = acc1 ++ acc2'

ndSum :: PoAM -> PoAM -> PoAM
ndSum m1 m2 = aePmod (ndSum' m1 m2)

zero :: PoAM
zero = Pmod [] [] [] []

ndS :: [PoAM] -> PoAM
ndS = foldl ndSum zero

testAnnounce :: Form -> PoAM
testAnnounce form = ndS [ cmp [ test form, public form ],
                          cmp [ test (negation form),
                                public (negation form)] ]

revealac_q = info [a,c] q
revealab_r = info [a,b] r
revealbc_p = info [b,c] p

initMuddy = upd (initE [P 0, Q 0, R 0]) (test (Conj [Neg p,Neg q, r]))

initMud = upds initMuddy [revealac_q,revealab_r,revealbc_p]

atleast1d = Disj[p,q,r]

round0 = upd initMud (public atleast1d)

cKnows = Disj [K c r, K c (Neg r)]

round1 = upd round0 (public cKnows)

initMuddy1 = upd (initE [P 0, Q 0, R 0]) (test (Conj [Neg p, q, r]))

initMud1 = upds initMuddy1 [revealac_q,revealab_r,revealbc_p]

nround0 = upd initMud1 (public atleast1d)

bcKnowNot = Conj [Neg (K b q), Neg (K b (Neg q)),
                  Neg (K c r), Neg (K c (Neg r))]

nround1 = upd nround0 (public bcKnowNot)

nround2 = upd nround1 (public cKnows)

measure :: (Eq a,Ord a) => (Model a b,a) -> Maybe [Int]
measure (m,w) =
  let
    f           = filter (\ (us,vs) -> elem w us || elem w vs)
    g [(xs,ys)] = length ys - 1
  in
    case kd45 (domain m) (access m) of
      Just a_balloons -> Just
         ( [ g (f balloons) | (a,balloons) <- a_balloons  ])
      Nothing         -> Nothing

cards0 :: EpistM
cards0 = (Pmod [0..5] val acc [0])
  where
  val    = [(0,[P 1,Q 2,R 3]),(1,[P 1,R 2,Q 3]),
            (2,[Q 1,P 2,R 3]),(3,[Q 1,R 2,P 3]),
            (4,[R 1,P 2,Q 3]),(5,[R 1,Q 2,P 3])]
  acc    = [(a,0,0),(a,0,1),(a,1,0),(a,1,1),
            (a,2,2),(a,2,3),(a,3,2),(a,3,3),
            (a,4,4),(a,4,5),(a,5,4),(a,5,5),
            (b,0,0),(b,0,5),(b,5,0),(b,5,5),
            (b,2,2),(b,2,4),(b,4,2),(b,4,4),
            (b,3,3),(b,3,1),(b,1,3),(b,1,1),
            (c,0,0),(c,0,2),(c,2,0),(c,2,2),
            (c,3,3),(c,3,5),(c,5,3),(c,5,5),
            (c,4,4),(c,4,1),(c,1,4),(c,1,1)]

showABp :: PoAM
showABp = (Pmod [0,1,2] pre susp [0])
  where
  pre  = [(0,p1),(1,q1),(2,r1)]
  susp = [(a,0,0),(a,1,1),(a,2,2),
          (b,0,0),(b,1,1),(b,2,2),
          (c,0,0),(c,0,1),(c,0,2),
          (c,1,0),(c,1,1),(c,1,2),
          (c,2,0),(c,2,1),(c,2,2)]

revealAB = reveal [b] [p1,q1,r1]

result   = upd cards0 revealAB

sadzik :: PoAM
sadzik = (Pmod
          [0..7]
          [(0, Conj[p,q,r]), (1, Conj[p,Neg q,r]),
           (2, Conj[Neg p,q,r]), (3, Conj[Neg p,Neg q,r]),
           (4, Conj[p,q,Neg r]), (5, Conj[p,Neg q,Neg r]),
           (6, Conj[Neg p,q,Neg r]), (7, Conj[Neg p,Neg q,Neg r])]
          ([(a,s,s) | s <- [0..7]] ++
             [(a,0,3),(a,3,0),(a,0,5),(a,5,0),(a,3,5),(a,5,3),
              (a,1,2),(a,2,1),(a,6,7),(a,7,6)] ++
              [(b,s,s) | s <- [0..7]] ++
                [(b,0,5),(b,5,0),(b,1,3),(b,3,1),(b,1,6),(b,6,1),(b,3,6),(b,6,3),
                 (b,2,4),(b,4,2),(b,2,7),(b,7,2),(b,4,7),(b,7,4)]) [0])

tnirwana = initE [P 0]
tn2fn    = message a p
fnirwana = upd tnirwana tn2fn
fn2tn    = public (Neg (K a p))

mm = public (Neg (K a bot))
