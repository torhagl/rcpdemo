\documentclass[ukenglish]{article}

\usepackage{mathptm}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amssymb} % for \mathbb
\usepackage{url}
\usepackage{color}
\usepackage{ulem}



\newtheorem{definition}{Definition}
\newtheorem{result}{Result}
\newtheorem{theorem}{Theorem}
\newcommand{\tor}[1]{\textcolor{cyan}{(Tor: #1)}}
\newenvironment{itquote}
  {\begin{quote}\itshape}
  {\end{quote}\ignorespacesafterend}

\author{}
\title{An 8-hand protocol for the Russian Cards Problem}
\begin{document}
\maketitle
The original Russian Cards Problem is described as follows:
\begin{itquote}
From a pack of seven known cards, two players each draw three cards and a third player gets the remaining card. How can the players with three cards openly (publicly) inform each other about their cards, without the third player learning from any of their cards who holds it?
\end{itquote}
\section{Definitions}
\begin{itemize}
\item  $a,b$ = Amount of cards held by agent a or b. When used in a text-setting, it simply means agent a or b.
\item  $eve$ = amount of cards held by the eavesdropper.
\item  $D$ = The deck of cards.
\item  $A, B$ = The set of A/Bs cards.
\item  $An$ = An announcement, a collection of alternative hands.
\item  $H_{An}$ = An alternative hand contained in announcement $An$. $H_{An} \subseteq D$, The cardinality of $H_{An}$ is determined by the size of the hand of the agent making the announcement $An$.
\item Protagonist = An agent that is going to be informed of the card deal.
\item Eavesdropper = The agent that is trying to learn the card deal, but is not meant to learn anything.
\item "A solution" = A sequence of announcements by the protagonists s.t. the eavesdropper has no knowledge of who holds any card and when the sequence terminates, all protagonists have common knowledge of the card deal.
\end{itemize}
Postconditions are defined in the following way:
\[ eveIgnorant = \bigwedge_{d \in D} (\neg K_{eve} d_a \wedge \neg K_{eve} d_b) \]
\[ bKnowsAs = \bigwedge_{d\in D} (K_b d_a \vee K_b \neg d_a) \]
\[ aKnowsBs = \bigwedge_{d\in D} (K_a d_b \vee K_a \neg d_b) \]
\section{An 8-hand solution}
All models used when checking this are found in the appendix. I will be using $C_{ab}$ instead of $C_{\{ a,b\}}$ for convenience.
Presenting an 8-hand direct exchange:
\[8An = K_a(034_a \vee 035_a \vee 036_a \vee 045_a \vee 134_a \vee 135_a \vee 246_a \vee 012_a) \]
Terminating the protocol is done with the following announcement:
\[ K_b(6_{eve}) \]
There is an interesting thing to notice about this announcement. We have:
\[ M,0 \models K_a[8An] C_{ab} bKnowsAs \]
\[ M,0 \models K_b[8An] C_{ab} bKnowsAs\]
\[ M,0 \not\models C_{ab}[8An] C_{ab} bKnowsAs\]
Both protagonists know individually, in the initial model, that the announcement results in full informativity of $b$. However, they do not have common knowledge, in the initial model, of that fact. In the 102 solutions provided in [1], it is the case that they have common knowledge of it, specifically:
\[ M,0 \models C_{ab}[\varphi] C_{ab} bKnowsAs \]
Where $\varphi$ is any arbitrary announcement of the 102 listed in [1]. This is because the models updates with one of the 102 announcements have strictly singleton equivalence classes for $b$. This is not the case for the 8-hand announcement - and is the reason the following holds:
\[ M,0 \models \neg K_{eve}[8An] C_{ab}bKnowsAs\]
The above formula does not hold in any of the 102 cases listed in [1].
\newline It is also the case that all agents have common knowledge, in the initial model, that after the initial 8-hand announcement, Eve is ignorant.
\[ M,0 \models C_{Ag}[8An] C_{Ag} eveIgnorant \]
All postconditions are satisfied using the 8-hand protocol:
\[M,0 \models [8An][K_b6_{eve}]C_{Ag} eveIgnorant \]
\[M,0 \models [8An][K_b6_{eve}]C_{ab} aKnowsBs \]
\[M,0 \models [8An][K_b6_{eve}]C_{ab} bKnowsAs \]
We also have the following:
\[M,0 \models C_{Ag}[8An][K_b6_{eve}]C_{Ag} eveIgnorant \]
To sum up the results:
\begin{itemize}
  \item Both protagonists know individually, in the initial model, that after $8An$, we have: $C_{ab}bKnowsAs$
\item Eve does \textbf{not} know, in the initial model, that after $8An$, we have: $C_{ab}bKnowsAs$
\item All agents have common knowledge, in the initial model, that after $8An$, we have: $C_{Ag}eveIgnorant$
\item All agents have common knowledge, in the initial model, that after $8An$ and $K_b6_{eve}$, we have: $C_{Ag}eveIgnorant$
\end{itemize}
This 8-hand announcement goes against many of the theorems around the Russian Card Puzzle, as they are proved under the following assumption:
\[ [An]C_{Ag}bKnowsAs \]
However, the following weakened version of the above formula does not change anything when trying solve the original formulation of the problem:
\[ [An]C_{ab}bKnowsAs \]
Adhering only to this weakened requirement, Eve actually gets less information during the exchange. The following propositions do not hold with this weaker requirement.
\begin{itemize}
  \item Prop 31 [1]: No direct exchange consists of more than seven hands. This one consists of 8.
  \item Prop 32 [1]: No direct exchange contains crossing hands. (Meaning that two alternative hands share a pair of cards). In the announcement we have the pair (0,3) in 3 alternative hands, (1,3) in 2 alternative hands, (0,5) in two alternative hands, (0,4) in two alternative hands, (3,4) in two alternative hands, and (3,5) in two alternative hands.
  \item Prop 33 [1]: Every card occurs at most thrice in a direct exchange. The card 3 occurs 5 times.
\end{itemize}
It is important to note that Proposition 32 and 33 is proved on the basis of Proposition 31 - which is why they are also not true in this case.
\section{Crossing Hands in the Russian Cards Problem}
Crossing hands in [1] is defined in the following way: "Hands that have 2 cards in common". The following is a formalization of proposition 32:
\[\forall H_{An} \in An, |H_{An} \cap ArbH_{An}| < 2\]
Where $ArbH_{An}$ = Any arbitrary alternative hand in $An$. $ArbH_{An}$ means "Arbitrary Hand in $An$".
Consider two S5-models, the original model of (3,3,1) and its updated version, respectively:
\[M = (S, Ag, V, \sim, Prop)\]
\[M|An = (S', Ag, V', \sim', Prop)\]
\begin{theorem}
  $\forall H_{An} \in An$: $|H_{An} \cap ArbH_{An}| < 2 \Leftrightarrow \forall x,y \in S': (x,y) \in \sim_b' \rightarrow x = y$
\end{theorem}
$\Rightarrow$
Proof by contrapositivity:
\begin{itemize}
  \item  Assumption: $\exists x,y \in S'$ s.t. $(x,y) \in \sim_b'$ and $x \neq y$
\end{itemize}
From our assumption we have that there exists two states, $x,y$ that $b$ deem indistinguishable. The valuation in these two states must then be of the following form:
\begin{itemize}
  \item V(s) = $ijk_a \wedge lmn_b \wedge o_{eve}$
  \item V(t) = $ijo_a \wedge lmn_b \wedge k_{eve}$
\end{itemize}
Or some similar permutation of the variables, but $lmn_b$ must remain the same for $b$ to consider them indistinguishable. This means that $a$ will have two of the same variables in states $s$ and $t$, as $b$'s cards must remain the same and $eve$ can only differ with one card. $t$ and $s$ would not be part of $M|An$ unless $a$ had both $ijk_a$ and $ijo_a$ in her announcement, and the antecedent is therefore false as $|\{ ijk \} \cap \{ ijo \}| = 2$. Proof by contrapositivity holds.
\newline\newline$\Leftarrow$
Proof by contradiction. Assumptions:
\begin{itemize}
  \item Assumption 1: $\forall x,y \in S': (x,y) \in \sim_b' \rightarrow x = y$
  \item Assumption 2: $\exists H_{An} \in An$: $|H_{An} \cap ArbH_{An}| \geq 2$
\end{itemize}
From assumption 2 we can, instead of $\geq 2$ just say = 2. If it is greater than two, namely 3, it will be removed because of idempotent law, as the announcement is a disjunction of alternative hands. It can never be larger than 3, as a's hand contains three cards and she can't announce an alternative hand that is larger than her actual hand. We therefore get:
\begin{itemize}
  \item Assumption 1: $\forall x,y \in S': (x,y) \in \sim_b' \rightarrow x = y$
  \item Assumption 2: $\exists H_{An} \in An$: $|H_{An} \cap ArbH_{An}| = 2$
\end{itemize}
From assumption 2 we get that in the announcement, there are two alternative hands that share two of the same cards. And as a consequence, in the resulting updated model, there will be two states with the following valuation:
\begin{itemize}
  \item V(s) = $ijk_a \wedge lmn_b \wedge o_{eve}$
  \item V(t) = $ijo_a \wedge lmn_b \wedge k_{eve}$
\end{itemize}
This is because Eve considers it possible that in the state where she has $o$ (or $k$), Anne's actual hand is $ijk$ (or $ijo$), and in that case Bob would not learn the deal, as he would not be able to distinguish those hands from each other. This leads to a contradiction, because of the fact that the states $s$ and $t$ have the same evaluation for Bob. He can not distinguish those states, and states $s$ and $t$ will be in the same equivalence class. We then have $(s,t) \in \sim'_b \wedge s\neq t$. Assumption 1 is therefore false, and the contradiction holds.
\begin{theorem}
  $M|An,s \models C_{Ag}bKnowsAs \Leftrightarrow \forall H_{An} \in An$: $|H_{An} \cap ArbH_{An}| < 2$
\end{theorem}
$\Rightarrow$:
Proof by direct proof:
\begin{itemize}
  \item Assumption 1: $M|An,s \models C_{Ag}bKnowsAs$
\end{itemize}
If it is common knowledge among all agents that Bob knows Anne's cards, then in every state in the model $M|An$, Bob must know Anne's cards. This means that the following must hold:
\[ \forall x,y \in S': (x,y) \in \sim_b' \rightarrow x = y \]
Applying the truth of the above formula to Theorem 1 gives us:
\[\forall H_{An} \in An: |H_{An} \cap ArbH_{An}| < 2 \]
Consequent must be true when assuming the truth of the antecedent, proof by direct proof holds.
\newline$\Leftarrow$:
Proof by contradiction:
\begin{itemize}
  \item Assumption 1: $\forall H_{An} \in An$: $|H_{An} \cap ArbH_{An}| < 2$
  \item Assumption 2: $M|An,s \models \neg C_{Ag}bKnowsAs$
\end{itemize}
From Assumption 2:\newline There exists a pair $(s,x)\in \bigcup_{a \in Ag}^*\sim_a$ s.t. $(x,y) \in \sim_b$ and $x \not= y$. That would mean that in some state $b$ is unsure of the deal. Essentially meaning that there exists an equivalence class for $b$ that is not singleton. Applying assumption 1 to Theorem 1 gives the contradiction.
\subsection{You can't always remove crossing hands-states}
If we look at the final model of the 8-hand announcement protocol in the appendix, we see that we have the states [0,48,76,80] where $a$ and $b$ can distinguish between them all, and Eve cannot distinguish them from each other. Eve can not remove states 76 and 80, even though they have crossing hands. Eve can reason as follows for state 76:

"$a$ could have the cards $134$ and announced $8An$ giving a $50\%$ chance of a two-step protocol, and a $50\%$ chance of a longer protocol. (State 76 and 78 is singleton, where 77 and 79 are not.) Turns out it was a two-step protocol, but that would be achievable if $b$ had $025$. (State 76)"

And equivalently for state 80. The main point here is that $a$ is not announcing the length of the protocol with her first announcement. She is only announcing alternative hands. This can be shown if we extend the 8-hand announcement to a 9-hand announcement. Suppose we add a crossing hand with $a$'s actual hand.
\[9An = K_a(034_a \vee 035_a \vee 036_a \vee 045_a \vee 134_a \vee 135_a \vee 246_a \vee 012_a \vee 016_a)\]
This announcement has a $75\%$ chance of succeeding as far as $a$ can see. In states 1,2 and 3 $b$ would know the deal. However, in state 0 he would not. Because the actual state is 0, $b$ is unsure of the deal and instead of announcing Eve's card, as he doesn't know it, he can announce the following:
\[ omniAn_b = \textit{I could have any hand.} \]
A 35-hand announcement, stating that he could have any of the $(^7_3)$ possible combinations of hands. As $a$ hears this she realises $b$ did not learn the deal and announces the following six-hand announcement:
\[ 6An = K_a(034_a \vee 036_a \vee 045_a \vee 135_a \vee 246_a \vee 012_a) \]
$b$ then learns the deal, and the protocol is terminated with the following announcement:
\[ K_b6_{eve} \]
This sequence satisfies all postconditions, as we have:
\[M,0 \models [9An][omniAn_b][6An][K_b6_{eve}]C_{ab}(aKnowsBs \wedge bKnowsAs)\]
\[M,0 \models [9An][omniAn_b][6An][K_b6_{eve}]C_{Ag}(eveIgnorant)\]
Here the 9-hand announcement is constructed to not inform $b$ of the deal. The added disjunction is a crossing hand that contains $eve$'s card. If we instead extend it with one of $b$'s cards it is even more convincing that Eve cannot remove crossing states from the final model. Consider this slightly different 9-hand announcement:
\[9InfoAn = K_a(034_a \vee 035_a \vee 036_a \vee 045_a \vee 134_a \vee 135_a \vee 246_a \vee 012_a \vee 015_a)\]
Where instead of $016_a$ we use $015_a$. Here we have the following after the announcement:
\[M,0 \models [9InfoAn]bKnowsAs \]
It is important to note that $a$ does not know this, but does consider it possible:
\[M,0 \models [9InfoAn]\hat{K}_abKnowsAs\]
$a$ considers it to be a $75\%$ chance that $b$ learned the deal. As $b$ now knows the deal, he can announce eve's card and the protocol is terminated, satisfying all postconditions:
\[M,0 \models [9InfoAn][K_b6_{eve}]C_{ab}(aKnowsBs \wedge bKnowsAs)\]
\[M,0 \models [9InfoAn][K_b6_{eve}]C_{Ag}(eveIgnorant)\]
Leaving states [0,12,48,76,80] left in the model. The valuation in these states are the following:
\begin{verbatim}
c = eve
(0,[a0,a1,a2,b3,b4,b5,c6]), (12,[a0,a1,a5,b2,b3,b4,c6])
(48,[a0,a4,a5,b1,b2,b3,c6]), (76,[a1,a3,a4,b0,b2,b5,c6])
(80,[a1,a3,a5,b0,b2,b4,c6])
\end{verbatim}
As we see here, all states are crossing with some other state. If $eve$ were to eliminate all crossing states, there would be none left.
\section{Similarity to non-equivalence class specific solution}
If we look at Proposition 31, 32 and 33, and remove alternative hands in the 8-hand announcement such that they are satisfied, we get the following announcement which is a 'subset' of the 8-hand announcement presented earlier:
\[ 5HandAn = K_a(036_a \vee 045_a \vee 135_a \vee 246_a \vee 012_a) \]
This announcement is part of the 102 solutions listed in [1], and it also fulfills the axioms CA1, CA2 and CA3 from [2]. However, updating the initial model with this 5-hand announcement results in a slightly different model than when using the 8-hand one. State 76 is part of the final model when using the 8-hand announcement, whereas it is no longer considered possible in the 5-hand announcement. This is obvious, as $a$ is not announcing $134_a$ as an alternative hand in the 5-hand announcement. In fact, there are no announcements within the 102 solutions listed in [1] that results in the final model of the 8-hand announcement. State 76 and 80 are in the final model of the 8-hand protocol. These two states have crossing hands for $a$, and by proposition 32 there would not be two states with crossing hands for the 102 solutions listed, as crossing hands is not allowed to announce within those.
\section{Conclusion}
Many of the propositions in [1] are based off the following assumption:
\[M,s \models [An]C_{Ag}bKnowsAs \]
Using this assumption, you are excluding the possibility that $Eve$ might believe the protocol to be longer than a direct exchange. As I have shown previously, there are announcements that are known direct exchanges for agent $a$ and $b$, but this is not known by $Eve$. As such, requiring that any protocol must fulfill $C_{Ag}bKnowsAs$ after the first announcement gives the following: "If any agent initiates a protocol that have the possibility of being longer than a direct exchange (i.e contains crossing hands), all states where the announcement has the probability of being longer than a direct exchange are removed by Eve."

Is such an assumption important? I am not sure. To me it seems like a restriction you can apply if you want. It guarantees direct exchanges, and it guarantees that it always is a direct exchange for every hand in the announcement. If you don't assume $M,s \models [An]C_{Ag}bKnowsAs$, you open the possibility of direct exchanges where the eavesdropper would consider it possible that the direct exchange could exceed the length of a direct exchange, even though the communicating agents would know that it is a direct exchange.

\section{References}
[1] \indent Van Ditmarsch, Hans. "The russian cards problem." \textit{Studia logica 75.1} (2003): 31-62\newline
[2] \indent Albert, M. H., et al. "Safe communication for card players by combinatorial designs for two-step protocols." Australasian journal of Combinatorics 33 (2005): 33
\section{Appendix}
\subsection{Models}
Consider S5 models of the following form: $M = (S, Ag, V, \sim, Prop, 0)$\newline
\begin{verbatim}
Non-changing parts of the models:
c = eve.
Actual state: 0
Prop = {a0, b0, c0 .. a6,b6,c6}
Ag = {a,b,c}
V = [(0,[a0,a1,a2,b3,b4,b5,c6]),(1,[a0,a1,a2,b3,b4,b6,c5]),(2,[a0,a1,a2,b3,b5,b6,c4]),
(3,[a0,a1,a2,b4,b5,b6,c3]),(4,[a0,a1,a3,b2,b4,b5,c6]),(5,[a0,a1,a3,b2,b4,b6,c5]),
(6,[a0,a1,a3,b2,b5,b6,c4]),(7,[a0,a1,a3,b4,b5,b6,c2]),(8,[a0,a1,a4,b2,b3,b5,c6]),
(9,[a0,a1,a4,b2,b3,b6,c5]),(10,[a0,a1,a4,b2,b5,b6,c3]),(11,[a0,a1,a4,b3,b5,b6,c2]),
(12,[a0,a1,a5,b2,b3,b4,c6]),(13,[a0,a1,a5,b2,b3,b6,c4]),(14,[a0,a1,a5,b2,b4,b6,c3]),
(15,[a0,a1,a5,b3,b4,b6,c2]),(16,[a0,a1,a6,b2,b3,b4,c5]),(17,[a0,a1,a6,b2,b3,b5,c4]),
(18,[a0,a1,a6,b2,b4,b5,c3]),(19,[a0,a1,a6,b3,b4,b5,c2]),(20,[a0,a2,a3,b1,b4,b5,c6]),
(21,[a0,a2,a3,b1,b4,b6,c5]),(22,[a0,a2,a3,b1,b5,b6,c4]),(23,[a0,a2,a3,b4,b5,b6,c1]),
(24,[a0,a2,a4,b1,b3,b5,c6]),(25,[a0,a2,a4,b1,b3,b6,c5]),(26,[a0,a2,a4,b1,b5,b6,c3]),
(27,[a0,a2,a4,b3,b5,b6,c1]),(28,[a0,a2,a5,b1,b3,b4,c6]),(29,[a0,a2,a5,b1,b3,b6,c4]),
(30,[a0,a2,a5,b1,b4,b6,c3]),(31,[a0,a2,a5,b3,b4,b6,c1]),(32,[a0,a2,a6,b1,b3,b4,c5]),
(33,[a0,a2,a6,b1,b3,b5,c4]),(34,[a0,a2,a6,b1,b4,b5,c3]),(35,[a0,a2,a6,b3,b4,b5,c1]),
(36,[a0,a3,a4,b1,b2,b5,c6]),(37,[a0,a3,a4,b1,b2,b6,c5]),(38,[a0,a3,a4,b1,b5,b6,c2]),
(39,[a0,a3,a4,b2,b5,b6,c1]),(40,[a0,a3,a5,b1,b2,b4,c6]),(41,[a0,a3,a5,b1,b2,b6,c4]),
(42,[a0,a3,a5,b1,b4,b6,c2]),(43,[a0,a3,a5,b2,b4,b6,c1]),(44,[a0,a3,a6,b1,b2,b4,c5]),
(45,[a0,a3,a6,b1,b2,b5,c4]),(46,[a0,a3,a6,b1,b4,b5,c2]),(47,[a0,a3,a6,b2,b4,b5,c1]),
(48,[a0,a4,a5,b1,b2,b3,c6]),(49,[a0,a4,a5,b1,b2,b6,c3]),(50,[a0,a4,a5,b1,b3,b6,c2]),
(51,[a0,a4,a5,b2,b3,b6,c1]),(52,[a0,a4,a6,b1,b2,b3,c5]),(53,[a0,a4,a6,b1,b2,b5,c3]),
(54,[a0,a4,a6,b1,b3,b5,c2]),(55,[a0,a4,a6,b2,b3,b5,c1]),(56,[a0,a5,a6,b1,b2,b3,c4]),
(57,[a0,a5,a6,b1,b2,b4,c3]),(58,[a0,a5,a6,b1,b3,b4,c2]),(59,[a0,a5,a6,b2,b3,b4,c1]),
(60,[a1,a2,a3,b0,b4,b5,c6]),(61,[a1,a2,a3,b0,b4,b6,c5]),(62,[a1,a2,a3,b0,b5,b6,c4]),
(63,[a1,a2,a3,b4,b5,b6,c0]),(64,[a1,a2,a4,b0,b3,b5,c6]),(65,[a1,a2,a4,b0,b3,b6,c5]),
(66,[a1,a2,a4,b0,b5,b6,c3]),(67,[a1,a2,a4,b3,b5,b6,c0]),(68,[a1,a2,a5,b0,b3,b4,c6]),
(69,[a1,a2,a5,b0,b3,b6,c4]),(70,[a1,a2,a5,b0,b4,b6,c3]),(71,[a1,a2,a5,b3,b4,b6,c0]),
(72,[a1,a2,a6,b0,b3,b4,c5]),(73,[a1,a2,a6,b0,b3,b5,c4]),(74,[a1,a2,a6,b0,b4,b5,c3]),
(75,[a1,a2,a6,b3,b4,b5,c0]),(76,[a1,a3,a4,b0,b2,b5,c6]),(77,[a1,a3,a4,b0,b2,b6,c5]),
(78,[a1,a3,a4,b0,b5,b6,c2]),(79,[a1,a3,a4,b2,b5,b6,c0]),(80,[a1,a3,a5,b0,b2,b4,c6]),
(81,[a1,a3,a5,b0,b2,b6,c4]),(82,[a1,a3,a5,b0,b4,b6,c2]),(83,[a1,a3,a5,b2,b4,b6,c0]),
(84,[a1,a3,a6,b0,b2,b4,c5]),(85,[a1,a3,a6,b0,b2,b5,c4]),(86,[a1,a3,a6,b0,b4,b5,c2]),
(87,[a1,a3,a6,b2,b4,b5,c0]),(88,[a1,a4,a5,b0,b2,b3,c6]),(89,[a1,a4,a5,b0,b2,b6,c3]),
(90,[a1,a4,a5,b0,b3,b6,c2]),(91,[a1,a4,a5,b2,b3,b6,c0]),(92,[a1,a4,a6,b0,b2,b3,c5]),
(93,[a1,a4,a6,b0,b2,b5,c3]),(94,[a1,a4,a6,b0,b3,b5,c2]),(95,[a1,a4,a6,b2,b3,b5,c0]),
(96,[a1,a5,a6,b0,b2,b3,c4]),(97,[a1,a5,a6,b0,b2,b4,c3]),(98,[a1,a5,a6,b0,b3,b4,c2]),
(99,[a1,a5,a6,b2,b3,b4,c0]),(100,[a2,a3,a4,b0,b1,b5,c6]),(101,[a2,a3,a4,b0,b1,b6,c5]),
(102,[a2,a3,a4,b0,b5,b6,c1]),(103,[a2,a3,a4,b1,b5,b6,c0]),(104,[a2,a3,a5,b0,b1,b4,c6]),
(105,[a2,a3,a5,b0,b1,b6,c4]),(106,[a2,a3,a5,b0,b4,b6,c1]),(107,[a2,a3,a5,b1,b4,b6,c0]),
(108,[a2,a3,a6,b0,b1,b4,c5]),(109,[a2,a3,a6,b0,b1,b5,c4]),(110,[a2,a3,a6,b0,b4,b5,c1]),
(111,[a2,a3,a6,b1,b4,b5,c0]),(112,[a2,a4,a5,b0,b1,b3,c6]),(113,[a2,a4,a5,b0,b1,b6,c3]),
(114,[a2,a4,a5,b0,b3,b6,c1]),(115,[a2,a4,a5,b1,b3,b6,c0]),(116,[a2,a4,a6,b0,b1,b3,c5]),
(117,[a2,a4,a6,b0,b1,b5,c3]),(118,[a2,a4,a6,b0,b3,b5,c1]),(119,[a2,a4,a6,b1,b3,b5,c0]),
(120,[a2,a5,a6,b0,b1,b3,c4]),(121,[a2,a5,a6,b0,b1,b4,c3]),(122,[a2,a5,a6,b0,b3,b4,c1]),
(123,[a2,a5,a6,b1,b3,b4,c0]),(124,[a3,a4,a5,b0,b1,b2,c6]),(125,[a3,a4,a5,b0,b1,b6,c2]),
(126,[a3,a4,a5,b0,b2,b6,c1]),(127,[a3,a4,a5,b1,b2,b6,c0]),(128,[a3,a4,a6,b0,b1,b2,c5]),
(129,[a3,a4,a6,b0,b1,b5,c2]),(130,[a3,a4,a6,b0,b2,b5,c1]),(131,[a3,a4,a6,b1,b2,b5,c0]),
(132,[a3,a5,a6,b0,b1,b2,c4]),(133,[a3,a5,a6,b0,b1,b4,c2]),(134,[a3,a5,a6,b0,b2,b4,c1]),
(135,[a3,a5,a6,b1,b2,b4,c0]),(136,[a4,a5,a6,b0,b1,b2,c3]),(137,[a4,a5,a6,b0,b1,b3,c2]),
(138,[a4,a5,a6,b0,b2,b3,c1]),(139,[a4,a5,a6,b1,b2,b3,c0])])
\end{verbatim}
\begin{verbatim}
  ----------------------------------------------------------------------------
M =
S = [0..139]
EqClasses:
a:
{[0,1,2,3],[4,5,6,7],[8,9,10,11],[12,13,14,15],[16,17,18,19],[20,21,22,23],
[24,25,26,27],[28,29,30,31],[32,33,34,35],[36,37,38,39],[40,41,42,43],
[44,45,46,47],[48,49,50,51],[52,53,54,55],[56,57,58,59],[60,61,62,63],
[64,65,66,67],[68,69,70,71],[72,73,74,75],[76,77,78,79],[80,81,82,83],
[84,85,86,87],[88,89,90,91],[92,93,94,95],[96,97,98,99],[100,101,102,103],
[104,105,106,107],[108,109,110,111],[112,113,114,115],[116,117,118,119],
[120,121,122,123],[124,125,126,127],[128,129,130,131],[132,133,134,135],
[136,137,138,139]}

b:
{[0,19,35,75],[1,15,31,71],[2,11,27,67],[3,7,23,63],[4,18,47,87],[5,14,43,83],
[6,10,39,79],[8,17,55,95],[9,13,51,91],[12,16,59,99],[20,34,46,111],[21,30,42,107],
[22,26,38,103],[24,33,54,119],[25,29,50,115],[28,32,58,123],[36,45,53,131],
[37,41,49,127],[40,44,57,135],[48,52,56,139],[60,74,86,110],[61,70,82,106],
[62,66,78,102],[64,73,94,118],[65,69,90,114],[68,72,98,122],[76,85,93,130],
[77,81,89,126],[80,84,97,134],[88,92,96,138],[100,109,117,129],[101,105,113,125],
[104,108,121,133],[112,116,120,137],[124,128,132,136]}
c:
{[0,4,8,12,20,24,28,36,40,48,60,64,68,76,80,88,100,104,112,124],
[1,5,9,16,21,25,32,37,44,52,61,65,72,77,84,92,101,108,116,128],
[2,6,13,17,22,29,33,41,45,56,62,69,73,81,85,96,105,109,120,132],
[3,10,14,18,26,30,34,49,53,57,66,70,74,89,93,97,113,117,121,136],
[7,11,15,19,38,42,46,50,54,58,78,82,86,90,94,98,125,129,133,137],
[23,27,31,35,39,43,47,51,55,59,102,106,110,114,118,122,126,130,134,138],
[63,67,71,75,79,83,87,91,95,99,103,107,111,115,119,123,127,131,135,139]}
\end{verbatim}
\begin{verbatim}
  ----------------------------------------------------------------------------
M|8-Hand-An =
S = [0,1,2,3,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,
    76,77,78,79,80,81,82,83,116,117,118,119]
EqClasses:
a:
{[0,1,2,3],[36,37,38,39],[40,41,42,43],[44,45,46,47],[48,49,50,51],[76,77,78,79],
[80,81,82,83],[116,117,118,119]}
b:
{[0],[1],[2],[3],[36,45],[37,41,49],[38],[39,79],[40,44],[42],[43,83],[46],[47],
[48],[50],[51],[76],[77,81],[78],[80],[82],[116],[117],[118],[119]}
c:
{[0,36,40,48,76,80],[1,37,44,77,116],[2,41,45,81],[3,49,117],[38,42,46,50,78,82],
[39,43,47,51,118],[79,83,119]}
\end{verbatim}
\begin{verbatim}
  ----------------------------------------------------------------------------
M|8-Hand-An|K_b 6_c =
S = [0,48,76,80]
EqClasses:
a:
{[0],[48],[76],[80]}
b:
{[0],[48],[76],[80]}
c:
{[0,48,76,80]}
\end{verbatim}
\begin{verbatim}
  ----------------------------------------------------------------------------
M|9-Hand-An =
S = [0,1,2,3,16,17,18,19,36,37,38,39,40,41,42,43,44,45,46,47,48,
    49,50,51,76,77,78,79,80,81,82,83,116,117,118,119]
EqClasses:
a:
{[0,1,2,3],[16,17,18,19],[36,37,38,39],[40,41,42,43],[44,45,46,47],
[48,49,50,51],[76,77,78,79],[80,81,82,83],[116,117,118,119]}
b:
{[0,19],[1],[2],[3],[16],[17],[18,47],[36,45],[37,41,49],[38],[39,79],[40,44],
[42],[43,83],[46],[48],[50],[51],[76],[77,81],[78],[80],[82],
[116],[117],[118],[119]}
c:
{[0,36,40,48,76,80],[1,16,37,44,77,116],[2,17,41,45,81],[3,18,49,117],
[19,38,42,46,50,78,82],[39,43,47,51,118],[79,83,119]}
\end{verbatim}
\begin{verbatim}
  ----------------------------------------------------------------------------
M|9-Hand-An|OmniAn_b =
S = [0,1,2,3,16,17,18,19,36,37,38,39,40,41,42,43,44,45,46,47,48,
    49,50,51,76,77,78,79,80,81,82,83,116,117,118,119]
EqClasses:
a:
{[0,1,2,3],[16,17,18,19],[36,37,38,39],[40,41,42,43],[44,45,46,47],
[48,49,50,51],[76,77,78,79],[80,81,82,83],[116,117,118,119]}
b:
{[0,19],[1],[2],[3],[16],[17],[18,47],[36,45],[37,41,49],[38],[39,79],[40,44],
[42],[43,83],[46],[48],[50],[51],[76],[77,81],[78],[80],[82],
[116],[117],[118],[119]}
c:
{[0,36,40,48,76,80],[1,16,37,44,77,116],[2,17,41,45,81],[3,18,49,117],
[19,38,42,46,50,78,82],[39,43,47,51,118],[79,83,119]}
\end{verbatim}
\begin{verbatim}
  ----------------------------------------------------------------------------
M|9-Hand-An|Omni_b|6-Hand-An =
S = [0,1,2,3,36,37,38,39,44,45,46,47,48,49,50,51,80,81,82,83,116,117,118,119]
EqClasses:
a:
{[0,1,2,3],[36,37,38,39],[44,45,46,47],[48,49,50,51],
[80,81,82,83],[116,117,118,119]]
b:
{[0],[1],[2],[3],[36,45],[37,49],[38],[39],[44],[46],
[47],[48],[50],[51],[80],[81],[82],[83],[116],[117],[118],[119]}
c:
{[0,36,48,80],[1,37,44,116],[2,45,81],[3,49,117],
[38,46,50,82],[39,47,51,118],[83,119]}
\end{verbatim}
\begin{verbatim}
  ----------------------------------------------------------------------------
M|9-Hand-An|Omni_b|6-Hand-An|K_b 6_c =
S = [0,48,80]
EqClasses:
a:
{[0],[48],[80]}
b:
{[0],[48],[80]}
c:
{[0,48,80]}
\end{verbatim}
\begin{verbatim}
  ----------------------------------------------------------------------------
M|9-Info-Hand-An =
S = [0,1,2,3,12,13,14,15,36,37,38,39,40,41,42,43,44,45,46,
    47,48,49,50,51,76,77,78,79,80,81,82,83,116,117,118,119]
a:
{[0,1,2,3],[12,13,14,15],[36,37,38,39],[40,41,42,43],
[44,45,46,47],[48,49,50,51],[76,77,78,79],[80,81,82,83],[116,117,118,119]}
b:
{[0],[1,15],[2],[3],[12],[13,51],[14,43,83],[36,45],[37,41,49],
[38],[39,79],[40,44],[42],[46],[47],[48],[50],[76],[77,81],[78],
[80],[82],[116],[117],[118],[119]}
c:
{[0,12,36,40,48,76,80],[1,37,44,77,116],[2,13,41,45,81],
[3,14,49,117],[15,38,42,46,50,78,82],[39,43,47,51,118],[79,83,119]}
\end{verbatim}
\begin{verbatim}
  ----------------------------------------------------------------------------
M|9-Info-Hand-An|K_b 6_c =
S = [0,12,48,76,80]
EqClasses:
a:
{[0],[12],[48],[76],[80]}
b:
{[0],[12],[48],[76],[80]}
c:
{[0,12,48,76,80]}
\end{verbatim}
\begin{verbatim}
  ----------------------------------------------------------------------------
M|5-Hand-An =
S = [0,1,2,3,44,45,46,47,48,49,50,51,80,81,82,83,116,117,118,119]
EqClasses:
a:
{[0,1,2,3],[44,45,46,47],[48,49,50,51],[80,81,82,83],[116,117,118,119]])}
b:
{[0],[1],[2],[3],[44],[45],[46],[47],[48],[49],[50],[51],[80],[81],
[82],[83],[116],[117],[118],[119]}
c:
{[0,48,80],[1,44,116],[2,45,81],[3,49,117],[46,50,82],[47,51,118],[83,119]}
\end{verbatim}
\begin{verbatim}
  ----------------------------------------------------------------------------
M|5-Hand-An|K_b 6_c =
S = [0,48,80]
EqClasses:
a:
{[0],[48],[80]}
b:
{[0],[48],[80]}
c:
{[0,48,80]}
  ----------------------------------------------------------------------------
\end{verbatim}
\end{document}
