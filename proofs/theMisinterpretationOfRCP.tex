\documentclass[ukenglish]{article}

\usepackage{mathptm}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amssymb} % for \mathbb
\usepackage{url}
\usepackage{color}
\usepackage{ulem}



\newtheorem{definition}{Definition}
\newtheorem{result}{Result}
\newtheorem{theorem}{Theorem}
\newcommand{\tor}[1]{\textcolor{cyan}{(Tor: #1)}}
\newenvironment{itquote}
  {\begin{quote}\itshape}
  {\end{quote}\ignorespacesafterend}

\author{}
\title{Equivalence Class Specific Solutions in The Russian Cards Problem}

\begin{document}
\maketitle
The original Russian Cards Problem is described as follows:
\begin{itquote}
From a pack of seven known cards, two players each draw three cards and a third player gets the remaining card. How can the players with three cards openly (publicly) inform each other about their cards, without the third player learning from any of their cards who holds it?
\end{itquote}
Definitions:
\begin{itemize}
\item  $a,b$ = Amount of cards held by agent a or b. When used in a text-setting, it simply means agent a or b.
\item  $eve$ = amount of cards held by the eavesdropper.
\item  $D$ = The deck of cards.
\item  $A, B$ = The set of A/Bs cards.
\item  $An$ = An announcement, a collection of alternative hands.
\item  $H_{An}$ = An alternative hand contained in announcement $An$. $H_{An} \subseteq D$, The cardinality of $H_{An}$ is determined by the size of the hand of the agent making the announcement $An$.
\item Protagonist = An agent that is going to be informed of the card deal.
\item Eavesdropper = The agent that is trying to learn the card deal, but is not meant to learn anything.
\item "A solution" = A sequence of announcements by the protagonists s.t. the eavesdropper has no knowledge of who holds any card and when the sequence terminates, all protagonists have common knowledge of the card deal.
\end{itemize}
Consider the following model for the (3,3,1) RCP: $ M = (S, Ag, V, \sim, Prop)$ where:
\begin{itemize}
\item $S = [0..139]$
\item $Ag = \{ a,b,eve\}$
\item $V(0) = 012_a, 345_b, 6_{eve}$
\item $V(1) = 012_a, 346_b, 5_{eve}$
\item $V(2) = 012_a, 356_b, 4_{eve}$
\item $V(3) = 012_a, 456_b, 3_{eve}$
\item $Prop = \{ 0_a, 0_b, 0_c \ldots 6_a, 6_b, 6_c\}$
\end{itemize}
From [1] (a $b$-$set$ is a set of $b$ elements etc.):
\begin{itemize}
\item [\textbf{EA1:}] Whenever A can announce An, B knows A's hand after An.
\item [\textbf{EA2:}] Whenever A can announce An, C does not know any of A's cards after An.
\item [\textbf{EA3:}] Whenever A can announce An, C does not know any of B's cards after An.
\item [\textbf{CA1:}] For every $b$-$set$ X there is at most one member of An that avoids X.
\item [\textbf{CA2:}] For every $c$-$set$ X the members of An avoiding X have empty intersection.
\item [\textbf{CA3:}] For every $c$-$set$ X the members of An avoiding X have union consisting of all cards except those of X.
\end{itemize}
If we apply CA1, CA2 and CA3 to calculate announcements, we get 102 solutions to the Russian Cards Problem in the model described above. It is in fact 5862 solutions, not 102. The axiom CA1 is too strong. It is not necessary to check for every set that Bob can have, that there is only one $H_{An}$ that avoids it. A more correct version of CA1 can be constructed in the following way:
\[ \forall x \{ x | x\in D - A, |x| = b\} \textit{ there is at most one member of the announcement that avoids x.}\]
The reason we can remove A's set here, is because of the following:\newline
\[ M,0 \models K_a (012_a) \wedge K_a(345_b \vee 346_b \vee 356_b \vee 456_b)\]
It is possible to remove $a$'s set of cards from the initial announcement, because she knows her own cards, and she is the one constructing the announcement.
It is not possible to exclude $EVE$ as we have:
\[M,0 \models \hat{K}_a 6_b\]
Using this new version of CA1 does not provide a set of solutions, as this combined with CA2 and CA3 generates 14214 announcements - where 8352 are not solutions. Even though $b$ learns $a$'s cards (and therefore Eve's card) in all those 14214 announcements, the 8352 announcements that are not solutions do not satisfy the following condition:
\[ M,0 \models [An][K_b 6_{eve}]C_{Ag}eveIgnorant \]
where eveIgnorant is defined in the following way:
\[ eveIgnorant = \bigwedge_{d \in D} (\neg K_{eve} d_a \wedge \neg K_{eve} d_b) \]
It is important to note that even though we are describing a specific card-distribution of the Russian Cards Problem here, there is no loss of generality. The above holds in all equivalence classes of $a$, by changing the knowledge of $a$ corresponding to the equivalence class that the actual distribution falls under. $a$ \textbf{knows} which equivalence class she's in because she knows her own cards, and can therefore act accordingly. Every equivalence class for $a$ has 4 states, which is why only 4 evaluations are shown when describing the model.

Because the authors of [1] failed to take this into account, they also fail to see that the announcement contained in the footnote on page 4 is a solution. Even though they claim it is not. It satisfies all epistemic properties. The announcement they claim not to be a solution is the following:
\[ K_a(012_a \vee 034_a \vee 056_a \vee 135_a \vee 246_a \vee 235_a) \]
Their reasoning is that it does not satisfy EA2. This is false. In particular we have the following:
\[  M,0 \models [K_a(012_a \vee 034_a \vee 056_a \vee 135_a \vee 246_a \vee 235_a)]C_{Ag}eveIgnorant \]
as well as:
\[  M,0 \models K_a[K_a(012_a \vee 034_a \vee 056_a \vee 135_a \vee 246_a \vee 235_a)]C_{Ag}eveIgnorant \]
So it is the case that after the announcement, it is common knowledge that Eve is ignorant. It is also the case that $a$ \textbf{knows} this.
However, there is one thing this announcement does not fulfill. We have the following:
\[ M, 0 \not\models [K_a(012_a \vee 034_a \vee 056_a \vee 135_a \vee 246_a \vee 235_a)] C_{Ag}bKnowsAs \]
where bKnowsAs is defined in the following way:
\[ bKnowsAs = \bigwedge_{d\in D} (K_b d_a \vee K_b \neg d_a) \]
However, we have the following:
\[ M, 0 \models [K_a(012_a \vee 034_a \vee 056_a \vee 135_a \vee 246_a \vee 235_a)] C_{a,b}bKnowsAs \]
Terminating the protocol is done in the normal fashion - $b$ announces $eve$'s card. All postconditions are satisfied:
\[  M,0 \models [K_a(012_a \vee 034_a \vee 056_a \vee 135_a \vee 246_a \vee 235_a)][K_b 6_{eve}]C_{Ag}eveIgnorant \]
\[  M,0 \models [K_a(012_a \vee 034_a \vee 056_a \vee 135_a \vee 246_a \vee 235_a)][K_b 6_{eve}]C_{a,b}bKnowsAs \]
\[  M,0 \models [K_a(012_a \vee 034_a \vee 056_a \vee 135_a \vee 246_a \vee 235_a)][K_b 6_{eve}]C_{a,b}aKnowsBs \]
where aKnowsBs is defined in the same way as bKnowsAs:
\[ aKnowsBs = \bigwedge_{d\in D} (K_a d_b \vee K_a \neg d_b) \]
The slight difference here is that it is not common knowledge between all agents that bKnowsAs, Eve does not know that bKnowsAs after the initial announcement. This interaction is briefly mentioned as a footnote in [2] on page 56. Eve knowing bKnowsAs is not a necessity according to the original formulation of the problem and the announcement can therefore be categorized as an equivalence-class dependent solution. They say "Note that EA1 is indeed not satisfied, namely not for 135 and 235". That is true. However, $a$ would not announce this if she knew she was holding 135 or 235, as she would know that $b$ wouldn't learn the deal if that was the case. It is important to note that Eve does \textbf{not} know this. This is because she can't subtract A's set of cards when creating informative announcements for B, because she does not know A's hand.

The announcement described here would not be a solution in the equivalence classes of $a$ where she holds 135 or 235, but it is a solution when she holds 012. Here the fact that she knows her own cards is of utmost importance, and it is also an important aspect of the Russian Cards Problem in general.
\section{An 8-hand solution}
(Using $C_{ab}$ instead of $C_{\{ a,b\}}$ for convenience)
Presenting an 8-hand solution:
\[An = K_a(034_a \vee 035_a \vee 036_a \vee 045_a \vee 134_a \vee 135_a \vee 246_a \vee 012_a) \]
This initial announcement by $a$ satisfies all requirements of an initial announcement (see below). We terminate the protocol by using:
\[ K_b(6_{eve}) \]
There are several interesting things to notice about this announcement:
We have:
\[ M,0 \models K_a[An] C_{ab} bKnowsAs \]
\[ M,0 \models K_b[An] C_{ab} bKnowsAs\]
\[ M,0 \models \neg K_{eve}([An] C_{ab}bKnowsAs)\]
\[ M,0 \models C_{Ag}[An] C_{Ag} eveIgnorant \]
Both communicating agents know that the announcement is a fully informative announcement for $b$. Eve does not know that after the initial announcement, the communicating agents have common knowledge of $bKnowsAs$. All agents also have common knowledge that Eve will remain ignorant after the announcement. However, it is not the case that the communicating agents have common knowledge that the initial announcement is informative for $b$, even though they know it individually, as shown below:
\[ M,0 \not\models C_{ab}[An] C_{ab} bKnowsAs\]
All postconditions are satisfied:
\[M,0 \models [An][K_b6_{eve}]C_{Ag} eveIgnorant \]
\[M,0 \models [An][K_b6_{eve}]C_{ab} (aKnowsBs \wedge bKnowsAs) \]
We also have the following:
\[M,0 \models C_{Ag}[An][K_b6_{eve}]C_{Ag} eveIgnorant \]
To sum up the results:
\begin{itemize}
\item Both communicating agents know individually that after the initial announcement, we have: $C_{ab}bKnowsAs$
\item Eve does \textbf{not} know that after the initial announcement, we have: $C_{ab}bKnowsAs$
\item All agents have common knowledge that after the initial announcement, we have: $C_{Ag}eveIgnorant$
\item All agents have common knowledge that after both announcements, we have: $C_{Ag}eveIgnorant$
\end{itemize}
This initial announcement goes against many of the theorems around the Russian Card Puzzle:
\begin{itemize}
  \item Prop 31 [2]: No direct exchange consists of more than seven hands. This one consists of 8.
  \item Prop 32 [2]: No direct exchange contains crossing hands. (Meaning that two alternative hands share a pair of cards). In the announcement we have the pair (0,3) in 3 alternative hands.
  \item Prop 33 [2]: Every card occurs at most thrice in a direct exchange. The card 3 occurs 5 times.
\end{itemize}
\section{Models}
Here are the S5-models of the RCP, and how they evolve during the protocol:
\section{Further Work}
Equivalence-class specific solutions renders most theorems around the russian cards problem obsolete. We will see that many of the theorems presented in both [1] and [2] (if not all) are not applicable to equivalence-class specific solutions. Most interestingly, there are equivalence-class specific solutions that contain more alternative hands than the proven bounds of how many alternative hands there can be in a solution.
\section{References}
[1] \indent Albert, M. H., et al. "Safe communication for card players by combinatorial designs for two-step protocols." Australasian journal of Combinatorics 33 (2005): 33.\newline
[2] \indent Van Ditmarsch, Hans. "The russian cards problem." \textit{Studia logica 75.1} (2003): 31-62
\end{document}
