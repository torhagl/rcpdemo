module EpistemicSolutions
where

import RcpCalculations as Calc
import RcpBounds
import RcpModelGenerator
import RcpAxioms
import EpistemicProtocol as EpistmProt
import CombinatorialProtocol as CombProt

import DEMO_S5_extended

import Data.List as List
import Data.Set as Set
import Data.Map.Strict as Map

-- One of the "default" solutions for RCP 331.
a_announce :: Form a
a_announce = integerAnnouncementToProp [[0,1,2], [0,3,4], [0,5,6], [1,3,5], [2,4,6]] (Ag 0)

solutions_epistm_specifyLower_noCrossing :: [Int] -> Int -> Agent -> [Form Int]
solutions_epistm_specifyLower_noCrossing cardDstr lower agent = [addKnowledgeToAn u | u <- altHands, epistmSuccessNoCrossing (addKnowledgeToAn u)]
  where
      addKnowledgeToAn an = Kn agent (Disj an)
      altHands = EpistmProt.announcementCombinations actualHand (lower - 1) (List.delete actualHand (generateConjunctionsOfHands cardDstr agent))
      actualHand = EpistmProt.generateActualHand agent model
      epistmSuccessNoCrossing = EpistmProt.noCrossingHandsSuccess model
      model = generateRCPModel cardDstr

solutions_specifyLower_fromModel :: EpistM Int -> Int -> Agent -> [Form Int]
solutions_specifyLower_fromModel m lower agent = [addKnowledgeToAn u | u <- altHands, epistmSuccessNoCrossing (addKnowledgeToAn u)]
  where
    addKnowledgeToAn an = Kn agent (Disj an)
    altHands = EpistmProt.announcementCombinations actualHand (lower - 1) (List.delete actualHand (generateConjunctionsOfHands cardDstr agent))
    actualHand = EpistmProt.generateActualHand agent m
    epistmSuccessNoCrossing = EpistmProt.noCrossingHandsSuccess m
    cardDstr = getCardDistributionFromModel m

solutions_epistm_withCrossing :: [Int] -> Int -> Agent -> [Form Int]
solutions_epistm_withCrossing cardDstr lower (Ag x) = [EpistmProt.integerAnnouncementToProp u (Ag x) | u <- altHands, combSafety u, epistmCrossingInfo u] --,postconditionsRCP (finalmodel u)]
  where
    altHands = CombProt.getAllAlternativeHands actualHand (lower - 1) (List.delete actualHand (Calc.combinations (cardDstr !! x)  [0..sum cardDstr - 1]))
    actualHand = CombProt.generateActualHand (Ag x) cardDstr
    combSafety an = CombProt.axiomCA2CA3 cardDstr an eveInts
    epistmCrossingInfo an = EpistmProt.agentKnowsInformativity model (EpistmProt.integerAnnouncementToProp an (Ag x)) (Ag x)
    eveInts = List.map Set.fromList (Calc.combinations (last cardDstr) [0..sum cardDstr - 1])
    model = generateRCPModel cardDstr
    finalmodel an = upd_pa (upd_pa model (EpistmProt.integerAnnouncementToProp an (Ag x))) (announceEvesCards model (Ag 1))


solutions_epistm_withCrossingWeak :: [Int] -> Int -> Agent -> [Form Int]
solutions_epistm_withCrossingWeak cardDstr lower (Ag x) = [EpistmProt.integerAnnouncementToProp u (Ag x) | u <- altHands, combSafety u, epistmCrossingInfo u] --,postconditionsRCP (finalmodel u)]
  where
    altHands = CombProt.getAllAlternativeHands actualHand (lower - 1) (List.delete actualHand (Calc.combinations (cardDstr !! x)  [0..sum cardDstr - 1]))
    actualHand = CombProt.generateActualHand (Ag x) cardDstr
    combSafety an = CombProt.axiomCA2CA3 cardDstr an eveInts
    epistmCrossingInfo an = EpistmProt.agentKnowsInformativityWeak model (EpistmProt.integerAnnouncementToProp an (Ag x)) (Ag x)
    eveInts = List.map Set.fromList (Calc.combinations (last cardDstr) [0..sum cardDstr - 1])
    model = generateRCPModel cardDstr
    finalmodel an = upd_pa (upd_pa model (EpistmProt.integerAnnouncementToProp an (Ag x))) (announceEvesCards model (Ag 1))

solutions_epistm_lowerbound :: [Int] -> [Form Int]
solutions_epistm_lowerbound cardDstr
  | length cardDstr /= 3 = [x | x <- solutions_epistm_specifyLower_noCrossing cardDstr genLower a]
  | lower > upper = error "Lowerbound > upperbound. No twostep solution, exiting."
  | otherwise = [x | x <- solutions_epistm_specifyLower_noCrossing cardDstr lower a]
                          where (lower, upper) = threeAgentBounds [fromIntegral x' | x' <- cardDstr]
                                genLower = lowerBoundInitialPub cardDstr

solutions_epistm_all_withinBounds :: [Int] -> Agent -> [Form Int]
solutions_epistm_all_withinBounds cardDstr agent
 | length cardDstr /= 3 = concat [solutions_epistm_specifyLower_noCrossing cardDstr y agent | y <- genLowerToUpper]
 | otherwise = concat [solutions_epistm_specifyLower_noCrossing cardDstr y agent | y <- lowerToUpper]
  where lowerToUpper = [lower..upper]
        (lower, upper) = threeAgentBounds [fromIntegral x' | x' <- cardDstr]
        (genLower, genUpper) = genericBounds cardDstr
        genLowerToUpper = [genLower..genUpper]


solutions_epistm_onlySafety :: [Int] -> Int -> Agent -> [Form Int]
solutions_epistm_onlySafety cardDstr lower agent = [addKnowledgeToAn u | u <- altHands, epistmSuccessOnlySafe (addKnowledgeToAn u)]
  where
      addKnowledgeToAn an = Kn agent (Disj an)
      altHands = EpistmProt.announcementCombinations actualHand (lower - 1) (List.delete actualHand (generateConjunctionsOfHands cardDstr agent))
      actualHand = EpistmProt.generateActualHand agent model
      epistmSuccessOnlySafe = EpistmProt.onlySafetySuccess model
      model = generateRCPModel cardDstr


solutions_epistm_onlySafetyAfterFirst :: [Int] -> Int -> Agent -> [Form Int]
solutions_epistm_onlySafetyAfterFirst cardDstr lower agent = [addKnowledgeToAn u | u <- altHands, epistmSuccessOnlySafeAfterFirst (addKnowledgeToAn u)]
  where
      addKnowledgeToAn an = Kn agent (Disj an)
      altHands = EpistmProt.announcementCombinations actualHand (lower - 1) (List.delete actualHand (generateConjunctionsOfHands cardDstr agent))
      actualHand = EpistmProt.generateActualHand agent model
      epistmSuccessOnlySafeAfterFirst = EpistmProt.onlySafetySuccessAfterFirst model
      model = generateRCPModel cardDstr


getCardDistributionFromModel :: EpistM Int -> [Int]
getCardDistributionFromModel (Mo _ ag v _ _) = cardDstr
  where (_, val) = head $ Map.toList v
        cardDstr = List.map (\y -> length (List.filter (\(Prop p _) -> p == y) val)) ag