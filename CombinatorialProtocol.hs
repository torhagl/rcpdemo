module CombinatorialProtocol
where

import Data.List as List
import Data.Set as Set

import RcpCalculations as Calc
import DEMO_S5_extended

--Check what length the alternative hands collection will be in the `mod` solution is. The formula only works for the distribution (n,n,1)
sumModAlternativeHandsLength :: [Int] -> Int
sumModAlternativeHandsLength [anne,bob,eve]
  | anne == bob && eve == 1 = List.length [x | x <- Calc.combinations anne [0..anne+bob], List.sum x `mod` (2*anne+1) == realSum]
  | otherwise = error "sum mod 2n+1 solution is only applicable when Anne and Bob has the same amount of cards, and Eve has 1 card."
  where realSum = List.sum [0..anne - 1] `mod` (2*anne+1)
sumModAlternativeHandsLength _ = error "Sum mod 2n+1 solution only applicable for three agents."



-- START OF PAPER: 'Safe communication for card players by combinatorial designs for two-step protocols.'

noCrossingHandsSuccess :: [Int] -> [[Int]] -> [Set Int] -> [Set Int] -> Bool
noCrossingHandsSuccess cardDstr conjs bSets eveInts = axiomCA2CA3 cardDstr conjs eveInts && axiomCA1 conjs bSets

getAllAlternativeHands :: [Int] -> Int -> [[Int]] -> [[[Int]]]
getAllAlternativeHands ah 0 _ = [[ah]]
getAllAlternativeHands ah n hands = [y:ys | y:xs' <- List.tails hands, ys <- getAllAlternativeHands ah (n-1) xs']

generateActualHand :: Agent -> [Int] -> [Int]
generateActualHand (Ag x) cardDstr = [List.sum $ List.take x cardDstr..((cardDstr !! x) + List.sum (List.take x cardDstr)) -1]

axiomCA1 :: [[Int]] -> [Set Int] -> Bool
axiomCA1 announcement bsets = all (\y -> List.length [x | x <- handToSet, Set.intersection x y == Set.empty] <= 1) bsets
  where handToSet = List.map Set.fromList announcement


axiomCA2CA3 :: [Int] -> [[Int]] -> [Set Int] -> Bool
axiomCA2CA3 cardDstr hands eveInts =
  do
    let handToSet = List.map Set.fromList hands
    let allCards = Set.fromList [0..sum cardDstr - 1]
    let ca2 = all (\y -> List.foldl Set.intersection allCards (List.filter (not . Set.isSubsetOf y) handToSet) == Set.empty) eveInts
    let ca3 = all (\y -> Set.difference allCards y == Set.unions (List.filter (not . Set.isSubsetOf y) handToSet)) eveInts
    ca3 && ca2

-- "For every b-set X there is at most one member of L that avoids X"
-- Equivalent to "After initial announcement An, it is common knowledge among all agents that Bob knows Anne's cards.""
axiomCA1FromEpistM :: [Int] -> [Form a] -> Bool
axiomCA1FromEpistM cardDstr conjs = all (\y -> length [x | x <- formToInt, Set.intersection x y == Set.empty] <= 1) bsets
  where bsets = List.map Set.fromList (combinations (cardDstr !! 1) [0..sum cardDstr - 1])
        formToInt = [Set.fromList [x | Prp (Prop _ x) <- y] | (Conj y) <- conjs]

-- CA2: "For every c-set X the members of L avoiding X have empty intesection."
axiomCA2CA3FromEpistM :: [Int] -> [Form a] -> Bool
axiomCA2CA3FromEpistM cardDstr conjs =
  do
    let formToInt = [Set.fromList [x | Prp (Prop _ x) <- y] | (Conj y) <- conjs]
    let allCards = Set.fromList [0..sum cardDstr - 1]
    let eveInts = List.map Set.fromList (Calc.combinations (last cardDstr) (Set.toList allCards))
    let ca2 = all (\y -> List.foldl Set.intersection allCards (List.filter (not . Set.isSubsetOf y) formToInt) == Set.empty) eveInts
    let ca3 = all (\y -> Set.difference allCards y == Set.unions (List.filter (not . Set.isSubsetOf y) formToInt)) eveInts
    ca2 && ca3

-- "An announcement L satisfies axioms iff each distinct alternative hand has their intersection strictly less than a - c."
lemma1  :: (Eq a) => [Int] -> [Form a] -> Bool
lemma1 _ [] = True
lemma1 cardDstr (Conj x:xs) = all (\(Conj y) -> length (x `intersect` y) < head cardDstr - last cardDstr) xs && lemma1 cardDstr xs

-- "In a good announcement every point (card) lies in at least (eve + 1) alternative hands."
lemma3 :: [Int] -> [Form a] -> Bool
lemma3 cardDstr conjunctions = all (\y -> length (List.filter (\(Conj x) -> any (\(Prp (Prop _ i)) -> y == i) x) conjunctions) >= eveCardinality + 1) [0..sum cardDstr - 1]
  where eveCardinality = last cardDstr

-- END OF PAPER: 'Safe communication for card players by combinatorial designs for two-step protocols.'
