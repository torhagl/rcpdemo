module RcpBounds
where
import RcpCalculations

import Data.Set as Set
import Data.List as List

--Lowerbound is higher than upperbound, which makes it impossible to have a two-step protocol. The protocol must be three or more, or impossible to create.
threeSteps :: (RealFrac a) => Int -> [((Int,Int),[a])]
threeSteps highestVal = Set.toList $ Set.fromList $ [(threeAgentBounds [a,b,c],[a,b,c]) | [a,b,c] <- combinations 3 (makeCombinationList highestVal), lowerBoundThreeAgent [a,b,c] > upperBoundThreeAgent [a,b,c], c < b, c < (a-1)]

--Lowerbound is smaller than upperbound, which makes it possible to generate a two-step protocol. The two extra-criteria is Corollary 2 and Lemma 2 from Safe Communications yadayada two-step protocol. They are wrong because of colouring-protocol paper, I think.
twoSteps :: (RealFrac a) => Int -> [((Int,Int),[a])]
twoSteps highestVal = Set.toList $ Set.fromList [(threeAgentBounds [a,b,c],[a,b,c]) | [a,b,c] <- combinations 3 (makeCombinationList highestVal), lowerBoundThreeAgent [a,b,c] < upperBoundThreeAgent [a,b,c], c < b, c < (a-1)] -- Not generic (See combinations 3, static amount of agents.)

genericTwoSteps :: Int -> [((Int,Int),[Int])]
genericTwoSteps highestVal = Set.toList $ Set.fromList [(genericBounds [a,b,c],[a,b,c]) | [a,b,c] <- combinations 3 (makeCombinationList' highestVal), lowerBoundInitialPub [a,b,c] < upperBoundInitialPub [a,b,c], c < b, c < (a-1)] -- Not generic (See combinations 3, static amount of agents.)

makeCombinationList :: (RealFrac a) => Int -> [a]
makeCombinationList s = [fromIntegral x | x <- concat $ replicate 3 [1..s]]

makeCombinationList' :: Int -> [Int]
makeCombinationList' s = [fromIntegral x | x <- concat $ replicate 3 [1..s]]

threeAgentBounds :: (RealFrac a) => [a] -> (Int, Int)
threeAgentBounds cardDstr = (lower,upper)
  where lower = lowerBoundThreeAgent cardDstr
        upper = upperBoundThreeAgent cardDstr

-- Taking the maximum of the lowerbound formulae as that is most restrictive, making the amount of possible combinations of hands smaller.
lowerBoundThreeAgent :: (RealFrac a) => [a] -> Int
lowerBoundThreeAgent cardDstr = max (lowerBoundThreeAgent1 cardDstr) (lowerBoundThreeAgent2 cardDstr)
--Proposition 1, Safe communication for card players by combinatorial designs for two-step protocols.
lowerBoundThreeAgent2 :: (RealFrac a) => [a] -> Int
lowerBoundThreeAgent2 [a,b,c] = ceiling $ ((a + b + c) * (c + 1)) / a
--Proposition 2, Safe communication for card players by combinatorial designs for two-step protocols.
lowerBoundThreeAgent1 :: (RealFrac a) => [a] -> Int
lowerBoundThreeAgent1 [a,b,c] = ceiling $ ((a+b) * (a + b + c)) / (b * (b + c))

-- Taking the minimal lowerbound formulae as that gives it the highest restriction, making the amount of possible combinations of hands smaller.
upperBoundThreeAgent :: (RealFrac a) => [a] -> Int
upperBoundThreeAgent cardDstr = min (upperBoundThreeAgent1 cardDstr) (upperBoundThreeAgent2 cardDstr)
--Proposition 3, Safe communication for card players by combinatorial designs for two-step protocols.
upperBoundThreeAgent1 :: (RealFrac a) => [a] -> Int
upperBoundThreeAgent1 [a,b,c] = floor $ ((factorial (a+b+c) * factorial (c + 1)) / (factorial (b+c) * factorial (c + a + 1))) * fromIntegral (floor ((a+c+1) / (c + 1)))
--Proposition 4, Safe communication for card players by combinatorial designs for two-step protocols.
upperBoundThreeAgent2 :: (RealFrac a) => [a] -> Int
upperBoundThreeAgent2 [a,b,c] = floor $ ((factorial (a+b+c) * factorial (c + 1)) / (factorial a * factorial (b + (2*c) + 1))) * fromIntegral (floor ((b+(2*c)+1) / (c + 1)))

genericBounds :: [Int] -> (Int, Int)
genericBounds cardDstr = (lowerBoundInitialPub cardDstr, upperBoundInitialPub cardDstr)

{-
  From The Russian Cards Problem (Ditmarsch) - Proposition 30
  The where-clause comes from Proposition 29 (Every card occurs at least twice in a safe communication)
-}

lowerBoundInitialPub :: [Int] -> Int
lowerBoundInitialPub cardDstr = firstOver `div` sizeOfHand
    where minCards = 2 * sum cardDstr
          sizeOfHand = head cardDstr
          firstOver = head $ dropWhile (< minCards) [x*sizeOfHand | x <- [1..]]

{-
  From Theorem 5.1 and Prop 31 in Ditmarsch (2003): Crossing hands generalized is theorem 5.1. In proposition 31 they say that "There are (choose 7 2)=21 pairs of hands", and each hand where Alice holds 3 cards gives three pairs of cards 3(numberOfPairsPossibleInHand)*7(althands) = 21, 3(numberOfPairsPossibleInHand)*8(althands) = 24, therefore it cannot be more than 7 alternative hands . If we then generalize such that it is (choose |D| (a - c)) "pairs" of hands, then amountOfSubCrossingHands*(highestInt) =< (choose |D| (a - c)) should give a generic upperbound for a direct exchange.
-}
upperBoundInitialPub :: [Int] -> Int
upperBoundInitialPub cardDstr = last ints
  where numberOfxCrossings = choose (sum cardDstr) maximumAllowedCrossingCards
        maximumAllowedCrossingCards = head cardDstr - last cardDstr
        amountOfSubCrossingHands = length [x | x <- powerset [0..head cardDstr - 1], length x == maximumAllowedCrossingCards]
        ints = [x | x <- [0..numberOfxCrossings], x * (head cardDstr) <= numberOfxCrossings]
