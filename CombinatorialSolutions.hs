module CombinatorialSolutions
where

import RcpCalculations as Calc
import RcpBounds
import RcpModelGenerator
import RcpAxioms
import EpistemicProtocol as EpistmProt
import CombinatorialProtocol as CombProt

import DEMO_S5_extended

import Data.List as List
import Data.Set as Set
import Data.Map.Strict as Map

solutions_comb_specifyLower_noCrossing :: [Int] -> Int -> Agent -> [Form Int]
solutions_comb_specifyLower_noCrossing cardDstr lower (Ag x) = [EpistmProt.integerAnnouncementToProp u (Ag x) | u <- altHands, combSuccessNoCrossing u]
  where
    altHands = CombProt.getAllAlternativeHands actualHand (lower - 1) (List.delete actualHand (Calc.combinations (cardDstr !! x)  [0..sum cardDstr - 1]))
    actualHand = CombProt.generateActualHand (Ag x) cardDstr
    combSuccessNoCrossing an = CombProt.noCrossingHandsSuccess cardDstr an bsets eveInts
    bsets = List.map Set.fromList (Calc.combinations (cardDstr !! 1) [0..sum cardDstr - 1])
    eveInts = List.map Set.fromList (Calc.combinations (last cardDstr) [0..sum cardDstr - 1])

solutions_comb_lowerbound :: [Int] -> [Form Int]
solutions_comb_lowerbound cardDstr
  | length cardDstr /= 3 = [x | x <- solutions_comb_specifyLower_noCrossing cardDstr genLower a]
  | lower > upper = error "Lowerbound > upperbound. No twostep solution, exiting."
  | otherwise = [x | x <- solutions_comb_specifyLower_noCrossing cardDstr lower a]
                          where (lower, upper) = threeAgentBounds [fromIntegral x' | x' <- cardDstr]
                                genLower = lowerBoundInitialPub cardDstr

solutions_comb_all_withinBounds :: [Int] -> Agent -> [Form Int]
solutions_comb_all_withinBounds cardDstr agent
 | length cardDstr /= 3 = concat [solutions_comb_specifyLower_noCrossing cardDstr y agent | y <- genLowerToUpper]
 | otherwise = concat [solutions_comb_specifyLower_noCrossing cardDstr y agent | y <- lowerToUpper]
  where lowerToUpper = [lower..upper]
        (lower, upper) = threeAgentBounds [fromIntegral x' | x' <- cardDstr]
        (genLower, genUpper) = genericBounds cardDstr
        genLowerToUpper = [genLower..genUpper]

solutions_comb_onlySafe :: (Ord a) => [Int] -> Int -> Agent -> [Form a]
solutions_comb_onlySafe cardDstr lower (Ag x) = [Kn (Ag x) (Disj u) | u <- altHands, ax2 u]
  where altHands = announcementCombinations actualHand (lower - 1) (List.delete actualHand (generateConjunctionsOfHands cardDstr (Ag x)))
        actualHand = EpistmProt.generateActualHand (Ag x) model
        model = generateRCPModel cardDstr
        ax2 = axiomCA2CA3FromEpistM cardDstr --Preserve ignorance of Eve.
        -- use (lower - 1) because the actual hand is added at the last recursive step of announcementCombinations
