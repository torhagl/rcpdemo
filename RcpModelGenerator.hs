module RcpModelGenerator
where
import DEMO_S5_extended

import RcpCalculations as Calc

import Data.List as List
import Data.Set as Set
import Data.Map.Strict as Map

generateRCPModel :: [Int] -> EpistM Int
generateRCPModel cardDstr = Mo states agents val acc currentState
  where
    deals = generateStates cardDstr
    states = [0..amountOfStates cardDstr - 1]
    agents = generateAgentList cardDstr
    val = Map.fromList $ generateAllAgentProps deals
    acc = Map.fromList $ generateAcc deals agents
    currentState = [0]

generateStates :: [Int] -> [(Int, [[Int]])]
generateStates cardDstr = List.zip [0..] (generateHandCombinations cardDstr [0..lastCard])
  where lastCard = List.sum cardDstr - 1

generateHandCombinations :: [Int] -> [Int] -> [[[Int]]]
generateHandCombinations [] _ = [ [] ]
generateHandCombinations (h:hs) listOfCards = [y:ys | y:_ <- List.tails (Calc.combinations h listOfCards), ys <- generateHandCombinations hs (Set.toList (Set.difference (Set.fromList listOfCards) (Set.fromList y)))]

amountOfStates :: [Int] -> Int
amountOfStates [] = 1
amountOfStates (x:xs) = Calc.choose (List.sum (x:xs)) x * amountOfStates xs

generateAgentList :: [Int] -> [Agent]
generateAgentList cardDstr = [Ag x | x <- [0..length cardDstr - 1]]

generateAllAgentProps :: [(Int, [[Int]])] -> [(Int,[Prp])]
generateAllAgentProps [] = []
generateAllAgentProps ((st,deal):tls) = (st,prps) : generateAllAgentProps tls
  where prps = generatePropList (zip [0..] deal)

generatePropList :: [(Int, [Int])] -> [Prp]
generatePropList [] = []
generatePropList ((agent, cards):tls) = generateSingleProp agent cards ++ generatePropList tls

generateSingleProp :: Int -> [Int] -> [Prp]
generateSingleProp i = List.map $ Prop (Ag i)

generateAcc :: [(Int,[[Int]])] -> [Agent] -> [(Agent, Erel Int)]
generateAcc _ [] = []
generateAcc deals (h:hs) = (h, generateEqClasses h deals) : generateAcc deals hs

generateEqClasses :: (Eq t1) => Agent -> [(Int, [t1])] -> [[Int]]
generateEqClasses _ [] = []
generateEqClasses (Ag i) ((st, deal):states) = (st : [w | (w, _) <- fits]) : generateEqClasses (Ag i) rest
  where (fits, rest) = List.partition (\(_,y) -> (y !! i) == (deal !! i)) states
