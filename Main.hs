import Solutions
import RcpLatex as Latex
import DEMO_S5_extended

import Data.List as List
import Data.Time

main :: IO()
main = do
  let solving = CombSol.solutions_comb_all_withinBounds problem (Ag 0)
  time <- Data.Time.getZonedTime
  writeFile ("results/" ++ filename ++ "_" ++ show time ++ ".tex") (header ++ show (length solving){- ++ List.intercalate "\\newline" (List.map Latex.knowsToLatex solving) -}++ "\n\\end{document}")
  --writeFile ("results/" ++ filename ++ "_" ++ show time ++ ".result") (show solving)
  where header = "\\documentclass[ukenglish]{article}\\usepackage{amsmath}\\usepackage{amssymb}\\author{}\\title{"++filename++"}\\begin{document}\\maketitle\\noindent"
        filename = List.intercalate "" fname
        fname = List.map show problem
        problem = [3,3,1]
