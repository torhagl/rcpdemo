module RcpAxioms
where

import RcpCalculations
import DEMO_S5_extended

import Data.List as List
import Data.Set as Set
import Data.Map.Strict as Map
import EpistemicProtocol

checkAllInOne :: Form Int -> Form Int -> EpistM Int -> Bool
checkAllInOne firstAn sndAn m =
            isTrue m (Pub firstAn (CK comAgents (b_knows_as m))) &&
            isTrue m (Pub firstAn (CK ags (eveIgnorant m))) &&
            isTrue m (Pub firstAn (Pub sndAn (CK comAgents (Conj [b_knows_as m, a_knows_bs m])))) &&
            isTrue m (Pub firstAn (Pub sndAn (CK ags (eveIgnorant m))))
  where (Mo _ ags _ _ _) = m
        comAgents = take (length ags - 1) ags

{- THESE ARE EPISTEMIC AXIOMS -}
--checkAll :: EpistM Int -> Form Int -> Bool
--checkAll m an = check2 m an  && check4 m an && check5 m an && check6 m an && check7 m an && agentKnowsSafety m an (Ag 0) && annKnowsInformativity m an

annKnowsInformativity :: EpistM Int -> Form Int -> Bool
annKnowsInformativity m an = isTrue m (Kn (Ag 0)(Pub an (CK comAgents (b_knows_as m))))
  where (Mo _ ags _ _ _) = m
        comAgents = take (length ags - 1) ags

checkSafetyAndInformativity :: EpistM Int -> Form Int -> Bool
checkSafetyAndInformativity m an = check4 m an && (not $ List.null $ whichAgentsKnowDeal (upd_pa m an))
{- THESE ARE PERFECT AXIOMS, THEY WORK REGARDLESS OF ANYTHING -}
--After some announcement, all communicating agents know that b knows a's cards in model mod.
check2 :: EpistM Int -> Form Int -> Bool
check2 mod announcement = isTrue mod (Pub announcement (CK comAgents (b_knows_as mod)))
  where (Mo _ ags _ _ _) = mod
        comAgents = take (length ags - 1) ags


comKnowsBs :: EpistM Int -> Bool
comKnowsBs mod = isTrue mod (Conj[Disj[CK comAgs (Prp (Prop b i)), CK comAgs (Ng(Prp (Prop b i)))] | i<-[0..length v -1]])
  where comAgs = take (length ags - 1) ags
        (Mo _ ags val _ [st]) = mod
        v = val ! st



whichAgentsKnowDeal :: EpistM Int -> [Agent]
whichAgentsKnowDeal mod = [x | x <- ags, agentKnowsDeal mod x]
  where (Mo _ ags _ _ _) = mod

agentKnowsDeal :: EpistM Int -> Agent -> Bool
agentKnowsDeal mod ag = all (isTrue mod) [Conj[Disj[Kn ag (Prp (Prop x i)), Kn ag (Ng(Prp (Prop x i)))] | i<-[0..length v -1]] | x <- comAgs]
  where v = val ! st
        comAgs = take (length ags - 1) ags
        (Mo _ ags val _ [st]) = mod

{- THESE ARE THREE AGENT AXIOMS, THEY ONLY WORK FOR THE THREE AGENT CASE -}
{- THESE ARE FOUR AGENT AXIOMS, THEY ONLY WORK FOR THE FOUR AGENT CASE -}

{- THESE ONLY WORK FOR THE THREE AND FOUR AGENT CASE -}

{- THESE I AM UNSURE OF -}

-- Checking for knowledge during the communication:
--Step 1
-- After the first announcement, b knows a's cards. This only holds if it is a two-step solution
--check1 :: Form a -> EpistM Int -> Bool
--check1 announcement mod = isTrue mod (Pub announcement (b_knows_as mod))

-- And Bill knows Cath's card. (This is entailed by the fact that Bill knows Anne's cards.)
-- This criteria only holds if it is a two-step solution.
check3 :: EpistM Int -> Form Int  -> Bool
check3 mod announcement = isTrue mod (Pub announcement (announceEvesCards mod (Ag 1)))

-- Cath remains ignorant of anne's and Bill's cards, and this is common knowledge.
check4 :: EpistM Int -> Form Int -> Bool
check4 mod announcement = isTrue mod (Pub announcement (CK ags $ eveIgnorant mod))
  where (Mo _ ags _ _ _) = mod

--Step 2
-- After Bill announces Cath's card, Anne knows Bill's cards, with Cath remains ignorant.
check5 :: EpistM Int -> Form Int -> Bool
check5 mod announcement = isTrue mod (Pub announcement (Pub (announceEvesCards mod (Ag 1)) (CK comAgents $ a_knows_bs mod)))
  where (Mo _ ags _ _ _) = mod
        comAgents = take (length ags - 1) ags

check6 :: EpistM Int -> Form Int -> Bool
check6 mod announcement = isTrue mod (Pub announcement (Pub (announceEvesCards  mod (Ag 1)) (CK comAgents $ b_knows_as mod)))
  where (Mo _ ags _ _ _) = mod
        comAgents = take (length ags - 1) ags


check7 :: EpistM Int -> Form Int -> Bool
check7 mod announcement = isTrue mod (Pub announcement (Pub (announceEvesCards mod (Ag 1)) (CK ags $ eveIgnorant mod)))
  where (Mo _ ags _ _ _) = mod


{- THESE ARE COMBINATORIC AXIOMS -}

{- THESE ARE PERFECT AXIOMS, THEY WORK REGARDLESS OF ANYTHING -}
--Proven in results/directexchangesgeneralized.tex
minProtocolLength :: [Int] -> Int
minProtocolLength xs = length xs - 1


{-
  This is a generic two-step protocol axiom. If every communicating agent are to learn the deal, then at most one hand can avoid any of their sets. It is an extension of CA1, pretty sure it holds. Here it is enough to say that one communicating player should know the deal.
  TODO figure out if it is actually so.
-}
axiomCA1generic :: [Int] -> [Form a] -> Bool
axiomCA1generic cardDstr conjs = any (\z -> all (\y -> length [x | x <- formToInt, Set.intersection x y == Set.empty] <= 1) z) comSets
  where formToInt = [Set.fromList [x | Prp (Prop _ x) <- y] | (Conj y) <- conjs]
        comSets = getCommunicationAgentCombinationSets cardDstr

{- THESE ARE FOUR AGENT AXIOMS, THEY ONLY WORK FOR THE FOUR AGENT CASE -}

getCommunicationAgentCombinationSets :: [Int] -> [[Set Int]]
getCommunicationAgentCombinationSets cardDstr = [List.map Set.fromList (combinations x [0..sum cardDstr - 1]) | x <- tail $ take (length cardDstr - 1) cardDstr]

{- THESE ONLY WORK FOR THE THREE AND FOUR AGENT CASE -}

{- THESE I AM UNSURE OF -}

{-
  (Equal to Lemma 1 further down, only negative?)
  Theorem 5.1 in Additional constructions to solve the generalized russian cards problem using combinatorial designs.
  "The announcement A is informative for Bob if and only if there do not exist two distinct sets Ha, Ha' in A s.t. |Ha `ìntersect` Ha'| >= a-c"
-}
theorem51 :: (Eq a) => [Int] -> [Form a] -> Bool
theorem51 _ [] = True
theorem51 cardDstr (Conj x:xs) = not $ any (\(Conj y) -> length (x `intersect` y) >= head cardDstr - last cardDstr) xs && theorem51 cardDstr xs




-- Any card can at most be in three alternative hands.
atMostThrice :: (Eq a) => [Int] -> [Form a] -> Bool
atMostThrice cardDstr conjs = all (\y -> length (List.filter (Set.member y) formToInt) < 5) [0..length cardDstr -1]
  where formToInt = [Set.fromList [x | Prp (Prop _ x) <- y] | (Conj y) <- conjs]

{-
  Axioms CA1, CA2, CA3 from Safe Communication for Card Players by Combinatorial design for two-step protocols. They are proven equal to the epistemic success criteria for the three agent case.

  CA1 is deprecated, CA1generic works for the three-agent case.
-}



axiomCA1' :: [Int] -> [Form a] -> Bool
axiomCA1' cardDstr conjs = all (\y -> length [x | x <- formToInt, Set.intersection x y == Set.empty] <= 1) bsets
  where bsets = List.map Set.fromList (combinations (cardDstr !! 1) [head cardDstr..sum cardDstr - 1])
        formToInt = [Set.fromList [x | Prp (Prop _ x) <- y] | (Conj y) <- conjs]

axiomCA1a :: [Int] -> [Form a] -> Bool
axiomCA1a cardDstr conjs = all (\y -> length [x | x <- formToInt, Set.intersection x y == Set.empty] <= 1) bsets
  where bsets = List.map Set.fromList (combinations (cardDstr !! 0) [3,4,5,6])
        formToInt = [Set.fromList [x | Prp (Prop _ x) <- y] | (Conj y) <- conjs]
