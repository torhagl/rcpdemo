# README

### The Russian Cards Puzzle

The russian cards puzzle was first introduced in 1847, but got increased interest at the Moscow Mathematics Olympiad in 2000, hence its name, which was given by Hans Van Ditmarsch in an article from 2002. He thought the problem originated from the olympiad, and found out about Kirkman later. He then tried to change the name, but it had already stuck in the community. The original problem is described as follows:

> From a pack of seven known cards two players (Alice, Bob) each draw three cards and a third player (Eve) gets the remaining card. How can the players with three cards openly (publicly) inform each > other about their cards, without the third player learning from any of their cards who holds it.

### How do I get set up?

Clone the repository. Go into GHCi and do the following:

1. `:l Solutions.hs` This will load everything.
2. `solutions_comb_all_withinBounds [a,b,eve] a` Where a,b,eve are ints, determining how many cards agent a,b and eve have respectively, the `a` represents Anne.

This produces a list of all announcements that agent a can announce, where agent b announces eve's card after and Eve is still ignorant. To test the original problem, use `solutions_comb_all_withinBounds [a,b,eve] a`. It currently only supports two-step solutions.

### Who do I talk to?

- Tor Hagland, torhagl@gmail.com
